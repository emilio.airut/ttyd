#!/usr/bin/python3
import os, sys , pexpect

# Colores
reset_color = "\x1b[1;0m"
verde = "\x1b[1;92m"
rojo = "\x1b[1;91m"
amarillo = "\x1b[1;93m"
cyan = "\x1b[1;36m"
blanco = "\x1b[1;97m"
magenta = "\x1b[1;95m"
azul = "\x1b[1;94m"

def banner():
    # os.system("clear")
    print(chr(27) + "[2J")
    
    print("""%s
 ██████╗ █████╗ ████████╗██╗   ██╗    ██╗  ██╗██╗   ██╗ █████╗ ██╗    ██╗███████╗██╗
██╔════╝██╔══██╗╚══██╔══╝██║   ██║    ██║  ██║██║   ██║██╔══██╗██║    ██║██╔════╝██║
██║     ███████║   ██║   ██║   ██║    ███████║██║   ██║███████║██║ █╗ ██║█████╗  ██║
██║     ██╔══██║   ██║   ╚██╗ ██╔╝    ██╔══██║██║   ██║██╔══██║██║███╗██║██╔══╝  ██║
╚██████╗██║  ██║   ██║    ╚████╔╝     ██║  ██║╚██████╔╝██║  ██║╚███╔███╔╝███████╗██║
 ╚═════╝╚═╝  ╚═╝   ╚═╝     ╚═══╝      ╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═╝ ╚══╝╚══╝ ╚══════╝╚═╝
                                                                                    
            %s| by Emilio Airut. | version 1.1 | last update: 11-11-2021 | 
                        %s""" % (verde,azul,reset_color))

def menu():
    print("""%sOpciones de este menu:
        1) Habilitar CATV
        2) Deshabilitar CATV
        """ % (blanco))
    while True:
        eleccion = input("%s[*] Seleccione una opcion (quit para salir): %s" % (cyan,reset_color))
        if eleccion == "quit":
            print("%s[+] Gracias por usar el script!" % (magenta) )
            break
            sys.exit(1)
        elif eleccion == "1":
            print("%s[+] Habilitacion de CATV" % (verde) )
            seriales(eleccion)
            break
        elif eleccion == "2":
            print("%s[+] Deshabilitacion de CATV" % (rojo) )
            seriales(eleccion)
            break
        else:
            print("%s[*] Debes seleccionar una opcion del menu, intenta de nuevo" % (amarillo) )

def seriales(eleccion):
    while  True:
        seriales = input("%s[*] Introducir los seriales (ej: 48575443e566819b,48575443e565649b): %s" % (cyan,reset_color)).lower()
        if seriales:
            seriales = seriales.split(",")
            break

    for serie in seriales:
        if not len(serie) == 16 :
            print("%s[-] El valor %s no cumple con los 16 caracteres del numero de serie, por lo cual no se cargaran en la DB" % (amarillo,serie))
            seriales.remove(serie)
    # quitar_ont= []
    # for serie in seriales:
    #     if not len(serie) == 16 :
    #         print("%s[-] El valor %s no cumple con los 16 caracteres del numero de serie, por lo cual no se cargaran en la DB" % (amarillo,serie))
    #         quitar_ont.append(serie)

    if not seriales:
        print("%s[-] No hay seriales de 16 caracteres cargados. Salimos" % (rojo) )
        sys.exit(0)
    else:
        pre_db(seriales,eleccion)

def pre_db(seriales,eleccion):
    open("/tmp/ont_huawei_catv.txt","w").close()
    lista_seriales = []
    mysql_pass = "From\$hell#2003"
    for serial in seriales:
        serie = "HWTC" + serial[8::]
        lista_seriales.append(serie)
        archivo = open ( "/tmp/ont_huawei_catv.txt","a")
        if eleccion == "1":
            check_is_in_db = os.popen("mysql -p%s -e \"select * from radius.radreply where username = '%s' and value = 'video 1 disable' \" 2>&1 | grep id " % (mysql_pass,serie)).read()
            check_is_in_db_2 = os.popen("mysql -p%s -e \"select * from radius.radreply where username = '%s' and value = 'video 1 enable' \" 2>&1 | grep id " % (mysql_pass,serie)).read()
            if check_is_in_db and not check_is_in_db_2:
                archivo.write("""
        INSERT INTO `radius`.`radreply` (`username`, `attribute`, `op`, `value`) VALUES ('{serie}', 'Furukawa-Gpon-Onu-Uni-Port-Admin', '==', 'video 1 enable');
                    """.format(serie=serie))
                archivo.close()

            elif not check_is_in_db:
                archivo.write("""
        INSERT INTO `radius`.`radreply` (`username`, `attribute`, `op`, `value`) VALUES ('{serie}', 'Furukawa-Gpon-Onu-Uni-Port-Admin', '==', 'video 1 disable');
        INSERT INTO `radius`.`radreply` (`username`, `attribute`, `op`, `value`) VALUES ('{serie}', 'Furukawa-Gpon-Onu-Uni-Port-Admin', '==', 'video 1 enable');
                    """.format(serie=serie))
                archivo.close()

            else:
                print("{verde} ¡¡¡ La ont {serie} ya se encuentra con el CATV habilitado !!!".format(verde=verde,serie=serie))
                sys.exit(1)

        elif eleccion == "2":
            check_is_in_db = os.popen("mysql -p%s -e \"select * from radius.radreply where username = '%s' and value = 'video 1 disable' \" 2>&1 | grep id " % (mysql_pass,serie)).read()
            check_is_in_db_2 = os.popen("mysql -p%s -e \"select * from radius.radreply where username = '%s' and value = 'video 1 enable' \" 2>&1 | grep id " % (mysql_pass,serie)).read()
            if check_is_in_db and check_is_in_db_2:
                archivo.write("""
        DELETE FROM radius.radreply where username = '{serie}' and value = 'video 1 enable' """.format(serie=serie))
                archivo.close()
            else:
                print("{amarillo} ¡¡¡ La ont {serie} ya se encuentra con el CATV deshabilitado !!!".format(amarillo=amarillo,serie=serie))
                sys.exit(1)


    db(lista_seriales,eleccion)

def db(lista_seriales,eleccion):
    print("%s[+] Se esta escribiendo la DB por favor aguarde solo un momento..." %(blanco))
    mysql = os.popen('mysql -uroot -pFrom\$hell#2003 -e "$(cat /tmp/ont_huawei_catv.txt)" 2>&1 ').read()
    cantidad = 1
    for ont in lista_seriales:
        print("\t%s) %s" %(cantidad,ont))
        cantidad += 1
    print("\n%s[+] Se escribio la DB correctamente con los seriales detallados anteriormente" % (verde))
    os.remove("/tmp/ont_huawei_catv.txt")
    olt(lista_seriales,eleccion)


def olt(lista_seriales,eleccion):

    mysql_pass = "From\$hell#2003"

    ip_olt = os.popen("mysql -uroot -p%s -e 'select ip from aprosftthdata.olt' 2>&1 | egrep -v '(Warning|ip)'" %
                    mysql_pass).read().splitlines()
    usuario_olt = os.popen(
        "mysql -uroot -p%s -e 'select username from aprosftthdata.olt' 2>&1 | egrep -v '(Warning|username)'" % mysql_pass).read().splitlines()
    password_olt = os.popen(
        "mysql -uroot -p%s -e 'select password from aprosftthdata.olt' 2>&1 | egrep -v '(Warning|password)'" % mysql_pass).read().splitlines()
    enable_password_olt = os.popen(
        "mysql -uroot -p%s -e 'select enable_password from aprosftthdata.olt' 2>&1 | egrep -v '(Warning|enable_password)'" % mysql_pass).read().splitlines()
    name_olt = os.popen("mysql -uroot -p%s -e 'select name from aprosftthdata.olt' 2>&1 | egrep -v '(Warning|name)'" %
                        mysql_pass).read().splitlines()

    lista_olt = []
    for i in range(0, len(ip_olt)):
        dicc = lista_olt.append(["OLT%s" % i, ip_olt[i], usuario_olt[i],
                                password_olt[i], enable_password_olt[i], name_olt[i]])

    lista_ingresada = ['HWTC7667e29f',"HWTC7667e21f","HWTC7667e22f"]
    lista_ingresada2 = [' HWTCe566319b ', ' FISA83121101 ', ' HWTC7667e29f ']

    print("%s[*] Ingresando a OLT/s" % (azul))
    for i in lista_olt:
        if not lista_seriales:
            print("%s[*] No hay mas seriales para recorrer. Salimos... %s" % (amarillo,reset_color))
            sys.exit(1)
        lista_ont_para_reiniciar = []
        if i[4]:
            ip_olt = i[1]
            user_olt = i[2]
            pass_olt = i[3]
            ena_olt = i[4]
            try:
                os.system(">/tmp/expect-olt-%s.txt" % ip_olt)
                telconn = pexpect.spawn('telnet {}'.format(ip_olt))
                print("\n%s[+] Accediendo a OLT %s ...%s" %
                      (verde, ip_olt, reset_color))
                telconn.logfile = open('/tmp/expect-olt-%s.txt' % ip_olt, 'ab')
                telconn.expect(".* login:")
                telconn.send("%s" % user_olt + "\r")
                telconn.expect("Password:")
                telconn.send("{}".format(pass_olt) + "\r")
                telconn.expect(".*>")
                telconn.send("ena" + "\r")
                telconn.expect("Password:")
                telconn.send("%s" % ena_olt + "\r")
                telconn.expect(".#")
                telconn.send("show onu info| redirect results.txt" + "\r")
                telconn.expect(".*#")
                telconn.send("con t" + "\r")
                telconn.expect(".#")
                telconn.send("do q sh -l" + "\r")
                telconn.expect(".#")
                telconn.send("cat /etc/results.txt" + "\r")
                telconn.expect(".#")
                telconn.send("exit" + "\r")
                telconn.expect(".#")
                telconn.send("exit" + "\r")
                telconn.expect(".#")
                telconn.send("show system | include Model" + "\r")
                telconn.expect(".#")
                telconn.send("exit" + "\r")
                modelo_olt = os.popen(
                    "cat /tmp/expect-olt-%s.txt  | grep Name | cut -f 2 -d: | sed 's/ //'" % ip_olt).read()
                modelo_olt = modelo_olt.strip()
                #print("%s[+] OLT modelo %s" % (verde, modelo_olt))
            except:
                print("%s[-] No se puede conectar a la OLT %s %s" %
                      (rojo, ip_olt, reset_color))
                os.remove("/tmp/expect-olt-%s.txt" % ip_olt)

            if os.path.exists("/tmp/expect-olt-%s.txt" % ip_olt):
                a = os.popen('cat /tmp/expect-olt-%s.txt | grep \| | egrep -v "(OLT|Model|redirect)"' %
                            ip_olt).read().splitlines()
                lista_ont = []
                for i in a:
                    i = i.split("|")
                    lista_ont.append(i[3])
                for x in lista_ont:
                    x = x.replace(" ", "")
                    if x in lista_seriales:
                        lista_seriales.remove(x)
                        ont_encontrada = os.popen(
                            "cat /tmp/expect-olt-%s.txt | grep \| | egrep -v \"(OLT | Model)\" | grep %s | awk '{{print $1,$3}}'" % (ip_olt,x)).read().splitlines()
                        print("%s[+] Accediendo a OLT %s para aplicar cambios ...%s" % (verde, ip_olt, reset_color))
                        for mac in ont_encontrada:
                            mac = mac.split(" ")
                            placa_olt = mac[0]
                            id_ont = mac[1]
                            
                            telconn = pexpect.spawn('telnet {}'.format(ip_olt))
                            telconn.expect(".* login:")
                            telconn.send("%s" % user_olt + "\r")
                            telconn.expect("Password:")
                            telconn.send("{}".format(pass_olt) + "\r")
                            telconn.expect(".*>")
                            telconn.send("ena" + "\r")
                            telconn.expect("Password:")
                            telconn.send("%s" % ena_olt + "\r")
                            telconn.expect(".#")
                            telconn.send("con t" + "\r")
                            telconn.expect(".#")
                            telconn.send("gp" + "\r")
                            telconn.expect(".#")
                            telconn.send("gpon-olt %s" % placa_olt + "\r")
                            telconn.expect(".#")
                            if eleccion == "1":
                                print("%s[+] Habilitando CATV en %s con id %s y mac %s ...%s" % (verde, placa_olt, id_ont,x, reset_color))
                                telconn.send("onu port-admin %s uni video 1 enable" % id_ont + "\r")
                            if eleccion == "2":
                                print("%s[+] Deshabilitando CATV en %s con id y mac %s %s ...%s" % (verde, placa_olt, id_ont,x,reset_color))
                                telconn.send("onu port-admin %s uni video 1 disable" % id_ont + "\r")
                            telconn.expect(".#")
                            telconn.send("end" + "\r")
                            telconn.expect(".#")
                            telconn.send("exit" + "\r")

                if lista_seriales:
                    print("%s[*] Estas ONT no la encontramos en la OLT" % (amarillo))
                    cantidad = 1
                    for ont in lista_seriales:
                        print("\t%s) %s" %(cantidad,ont))
                        cantidad += 1
                    #print("%s[-] %s NO ESTA EN ESTA OLT %s O NO EXISTE" % (rojo,x,ip_olt))

    print("\n%s[+] Proceso finalizado. Presione ENTER para acceder nuevamente\n" % (verde))

if __name__ ==  "__main__":
    try:
        banner()
        menu()
    except KeyboardInterrupt:
        print("%sSe detecta ctrl+c .Saliendo\n" % (rojo))
        sys.exit(0)


#Fix version 1.1
#Se corrige lectura de lista_seriales
#Se agregan nuevas variables si la ont no tiene valores de video previamente