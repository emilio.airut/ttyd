#!/usr/bin/python3
# -*- coding: utf-8 -*-
# By Emilio Airut

#Colores
reset_color = "\x1b[1;0m"
verde = "\x1b[1;92m"
rojo = "\x1b[1;91m"
amarillo = "\x1b[1;93m"
cyan = "\x1b[1;36m"
blanco = "\x1b[1;97m"
magenta = "\x1b[1;95m"
azul = "\x1b[1;94m"



#Fecha actualizacion
fecha_actualizacion = "21-12-2021"


##### MAIN #####

class Main():
    def main(self,hostname):
            try:
                mysql_pass = "From\$hell#2003"
                ip_olt = os.popen("mysql -uroot -h 127.0.0.1 -p%s -e 'select ip from aprosftthdata.olt' 2>&1 | egrep -v '(Warning|ip)'" % mysql_pass).read()
                ips = ip_olt.splitlines()
                lista_ip_olt = []
                for ip in ips:
                    lista_ip_olt.append(ip)
                # REMOVER DUPLICADOS
                #lista_ip_olt = list(dict.fromkeys(lista_ip_olt))
                ips = sorted(lista_ip_olt, key = ipaddress.IPv4Address)
                lista_ips = []
                for ip in ips:
                    id_olt = os.popen("mysql -p%s -e \"SELECT id_olt_model FROM aprosftthdata.olt where ip = '%s';\" 2>&1 | egrep -v '(Warning|model)'" %(mysql_pass,ip)).read().strip()
                    olt_mark = os.popen("mysql -p%s -e \"SELECT id_olt_mark FROM aprosftthdata.olt_model where id_olt_model = '%s';\" 2>&1 | egrep -v '(Warning|mark)' " % (mysql_pass,id_olt)).read().strip()
                    if olt_mark == "8":
                        olt_huawei = "%s OLT_HUAWEI" %(ip)
                        lista_ips.append(olt_huawei)
                    else:
                        lista_ips.append(ip)
                usuario_olt = os.popen("mysql -uroot -h 127.0.0.1 -p%s -e 'select username from aprosftthdata.olt' 2>&1 | egrep -v '(Warning|username)'" % mysql_pass).read().splitlines()
                password_olt = os.popen("mysql -uroot -h 127.0.0.1 -p%s -e 'select password from aprosftthdata.olt' 2>&1 | egrep -v '(Warning|password)'" % mysql_pass).read().splitlines()
                enable_password_olt = os.popen("mysql -uroot -h 127.0.0.1 -p%s -e 'select enable_password from aprosftthdata.olt' 2>&1 | egrep -v '(Warning|enable_password)'" %mysql_pass).read().splitlines()
                name_olt = os.popen("mysql -uroot -h 127.0.0.1 -p%s -e 'select name from aprosftthdata.olt' 2>&1 | egrep -v '(Warning|name)'" % mysql_pass).read().splitlines()
                name_olt[0] = name_olt[0]+"\t\t"

            except:
                print("%s[-] NO HAY OLT EN DATABASE o LA CONTRASEÑA DE MYSQL NO ES LA HABITUAL\n%s" % (rojo,reset_color))
                sys.exit(0)
            
            print("%s#########################" % (verde))
            print("%s#                       #" % (verde))
            print("%s#    OLT encontradas    #" % (verde))
            print("%s#                       #" % (verde))
            print("%s#########################\n" % (verde))

            numero=1
            dicc_ip={}
            for ip in lista_ips:
                    ip = ip.rstrip()
                    print("{})IP: {}".format(numero,ip))
                    if "HUAWEI" in ip:
                        ip = ip.split(" ")
                        dicc_ip["{}".format(numero)] = "{}".format(ip[0])
                        numero+=1
                    else:
                        dicc_ip["{}".format(numero)] = "{}".format(ip)
                        numero+=1
            list_ip2 = dicc_ip.keys()
            list_ip3 = []
            for ip in dicc_ip:
                    list_ip3.append(ip)
            try:
                    confirmar = True
                    while confirmar:
                            eleccion = input("\n%s[*] Por favor seleccione una OLT (quit para salir): %s" % (cyan,reset_color))
                            if eleccion == "quit":
                                    confirmar = False
                                    print("%s[*] Muchas gracias por usar el script!%s" % (magenta,reset_color))
                                    Tools().borrar_archivos()
                                    sys.exit()                                
                            elif eleccion in list_ip3:
                                    confirmar = False
                                    ip_olt = dicc_ip[eleccion]
                                    print(dicc_ip[eleccion])

                                    for i in range(0,len(lista_ip_olt)):
                                        dicc = ["OLT%s"%i,lista_ip_olt[i],usuario_olt[i],password_olt[i],enable_password_olt[i],name_olt[i]]
                                        if dicc[1] == ip_olt:
                                            ip = dicc[1]
                                            user_olt = dicc[2]
                                            pass_olt = dicc[3]
                                            ena_olt = dicc[4]
                                            name_olt = dicc[5]

                            else:
                                    print("%s[-] Esa opcion no esta en el menu, intente de nuevo%s" % (amarillo,reset_color))
            except KeyboardInterrupt:
                    Tools().clear_pantalla()
                    print("%s\n[-]Detectamos interrupcion por teclado (ctrl+c).Empezando nuevamente..." % rojo)
                    Tools().banner(hostname)
                    Main().main(hostname)

            try:    
                Tools().clear_pantalla()
                Tools().banner(hostname)
                if not ena_olt:
                    from opciones_huawei import opciones_ont
                    opciones_ont(ip_olt,user_olt,pass_olt)
                    sys.exit(1)
                    #print("%s[*] Este script aun no soporta OLT HUAWEI" % (amarillo))
                    #os.system("bash -c 'read -s -n 1 -p \"\n%s[*] Presione una tecla para continuar...%s\"'" % (blanco,reset_color))
                    #Tools().clear_pantalla()
                    #Tools().banner(hostname)
                    #Main().main(hostname)

                else:
                    print("\n%s[+] Chequeando conectividad con la IP %s%s" % (verde,ip_olt,reset_color))
                    ping_ip = subprocess.call(["ping", "-c", "1", "-n", "-W", "1", ip_olt],stdout=subprocess.DEVNULL)
                    if ping_ip == 0 :
                            print("\n%s[+] Respuesta de ping OK, continuamos...%s" % (verde,reset_color))
                    else:
                            print("%s[-] No responde ICMP la IP: %s" % (rojo,ip_olt))
                            print("[-] Verificar que este habilitada respuestas ICMP dentro de la red %s\n" % reset_color)
                            os.system("bash -c 'read -s -n 1 -p \"%s[*] Presione una tecla para continuar...%s\"'" % (verde,reset_color))
                            Tools().clear_pantalla()
                            Tools().banner(hostname)
                            Main().main(hostname)
            except:
                sys.exit(1)                        
            
            Main().menu(hostname,ip_olt,user_olt,pass_olt,ena_olt,name_olt)


    def menu(self,hostname,ip_olt,user_olt,pass_olt,ena_olt,name_olt):
        print("""\n%s[+] Opciones del menu para OLT con IP {} {}\n
        %s1) Limpiar ONT inactivas
        2) Ver modelos de ONT
        3) Ver cantidad total de ONT ACTIVAS
        4) Factory Reset ONT por modelo
        5) Firmware de ONT por modelo
        6) Ver ONT fuera de rango > -27.0 dbm
        7) Descripcion de ONT por modelo
        8) Reiniciar ONT sin perfil en OLT
        9) Ver/Actualizar Firware de ONT por MAC
        10) Ver/Reiniciar ONT por plan
        11) Factory Reset ONT LD420-10R sin ip de gestion
        12) Comprobar firmware de ONT por modelo
        13) Ver MAC por modelo de ONT
        14) Reset por DOWNLOAD FAIL
        15) Backup OLT (solo Furukawa)
        \n0 )Volver para seleccionar otra OLT del listado%s""".format(ip_olt,name_olt) % (cyan,blanco,reset_color))        
        confirmar_menu = True
        while confirmar_menu:
            try:
                def Limpieza():
                    confirmar_menu = False
                    Tools().clear_pantalla()
                    Tools().banner(hostname)
                    print("%s[+] Comenzando LIMPIEZA DE ONT INACTIVAS %s" % (cyan,reset_color))
                    if Olt().conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt) == "ERROR":
                            Olt().error_conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
                    else:
                            Opciones().limpiar_inactivas(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
                            Tools().continuar(ip_olt,user_olt,pass_olt,ena_olt,name_olt)

                def Modelos():
                    confirmar_menu = False
                    Tools().clear_pantalla()
                    Tools().banner(hostname)
                    print("%s[+] Comenzando VER MODELOS %s" % (cyan,reset_color))
                    if Olt().conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt) == "ERROR":
                            Olt().error_conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)     
                    else:
                            Opciones().limpiar_inactivas(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
                            Opciones().ver_modelos(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
                            Tools().continuar(ip_olt,user_olt,pass_olt,ena_olt,name_olt)

                def Total():
                    confirmar_menu = False
                    Tools().clear_pantalla()
                    Tools().banner(hostname)
                    print("%s[+] Comenzando CANTIDAD TOTAL ONT %s" % (cyan,reset_color))
                    if Olt().conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt) == "ERROR":
                            Olt().error_conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)     
                    else:
                            if Opciones().limpiar_inactivas(ip_olt,user_olt,pass_olt,ena_olt,name_olt) == "TERMINAR":
                                    print("%s[-] Debes limpiar las ONT INACTIVAS para conocer la cantidad real de las ONT ACTIVAS %s" %(magenta,reset_color))
                                    Tools().borrar_archivos()
                                    Main().menu(hostname,ip_olt,user_olt,pass_olt,ena_olt,name_olt)                                      
                            else:
                                    Opciones().cant_ont(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
                                    Tools().continuar(ip_olt,user_olt,pass_olt,ena_olt,name_olt)

                def Reset():
                    confirmar_menu = False
                    Tools().clear_pantalla()
                    Tools().banner(hostname)
                    print("%s[+] Comenzando RESET POR MODELO %s " % (cyan,reset_color))
                    if Olt().conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt) == "ERROR":
                            Olt().error_conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)     
                    else:
                            Opciones().reset_ont(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
                            Tools().continuar(ip_olt,user_olt,pass_olt,ena_olt,name_olt)

                def Firmware():
                    confirmar_menu = False
                    Tools().clear_pantalla()
                    Tools().banner(hostname)
                    print("%s[+] Comenzando VER FIRMWARE POR MODELO %s" % (cyan,reset_color))
                    if Olt().conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt) == "ERROR":
                            Olt().error_conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)     
                    else:
                            Opciones().firmware_ont(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
                            Tools().continuar(ip_olt,user_olt,pass_olt,ena_olt,name_olt)

                def Rango():
                    confirmar_menu = False
                    Tools().clear_pantalla()
                    Tools().banner(hostname)
                    print("%s[+] Comenzando VER ONT FUERA DE RANGO %s" % (cyan,reset_color))
                    if Olt().conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt) == "ERROR":
                            Olt().error_conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)     
                    else:
                            Opciones().dbm(ip_olt,user_olt,pass_olt,ena_olt,name_olt)  
                            Tools().continuar(ip_olt,user_olt,pass_olt,ena_olt,name_olt)

                def Descripcion():
                    confirmar_menu = False
                    Tools().clear_pantalla()
                    Tools().banner(hostname)
                    print("%s[+] Comenzando DESCRIPCION DE ONT %s" % (cyan,reset_color))
                    if Olt().conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt) == "ERROR":
                            Olt().error_conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)     
                    else:
                            if Opciones().limpiar_inactivas(ip_olt,user_olt,pass_olt,ena_olt,name_olt) == "TERMINAR":
                                    print("%s[-] Debes limpiar las ONT INACTIVAS para conocer la description real de las ONT ACTIVAS%s" % (magenta,reset_color))
                                    Main().menu(hostname,ip_olt,user_olt,pass_olt,ena_olt,name_olt)                                      
                            else:
                                    Opciones().description(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
                                    Tools().continuar(ip_olt,user_olt,pass_olt,ena_olt,name_olt)

                def Sin_perfil():
                    confirmar_menu = False
                    Tools().clear_pantalla()
                    Tools().banner(hostname)
                    print("%s[+] Comenzando REINICIO DE ONT SIN PERFIL %s" % (cyan,reset_color))
                    if Olt().conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt) == "ERROR":
                            Olt().error_conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)     
                    else:
                        Opciones().sin_profile(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
                        Tools().continuar(ip_olt,user_olt,pass_olt,ena_olt,name_olt)

                def Actualizar_firmware():
                    confirmar_menu = False
                    Tools().clear_pantalla()
                    Tools().banner(hostname)
                    print("%s[+] Comenzando VER/ACTUALIZAR FIRMWARE POR MAC %s" % (cyan,reset_color))
                    if Olt().conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt) == "ERROR":
                            Olt().error_conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)     
                    else:
                        Opciones().actualizar_firmware(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
                        Tools().continuar(ip_olt,user_olt,pass_olt,ena_olt,name_olt)

                def Reiniciar_plan():
                    confirmar_menu = False
                    Tools().clear_pantalla()
                    Tools().banner(hostname)
                    print("%s[+] Comenzando VER/REINICIAR ONT POR PLAN %s" % (cyan,reset_color))
                    if Olt().conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt) == "ERROR":
                            Olt().error_conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)     
                    else:
                            Opciones().planes_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
                            Tools().continuar(ip_olt,user_olt,pass_olt,ena_olt,name_olt)

                def Sin_ip ():
                    confirmar_menu = False
                    Tools().clear_pantalla()
                    Tools().banner(hostname)
                    print("%s[+] Comenzando FACTORY RESET ONT LD420-10R SIN IP DE GESTION %s" % (cyan,reset_color))
                    if Olt().conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt) == "ERROR":
                            Olt().error_conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)     
                    else:
                        Opciones().ont_noip(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
                        Tools().continuar(ip_olt,user_olt,pass_olt,ena_olt,name_olt)

                def Check_firmware():
                    confirmar_menu = False
                    Tools().clear_pantalla()
                    Tools().banner(hostname)
                    print("%s[+] Comenzando COMPROBAR FIRMWARE POR MODELO %s" % (cyan,reset_color))
                    if Olt().conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt) == "ERROR":
                            Olt().error_conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)     
                    else:
                            Opciones().check_firmware_ont(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
                            Tools().continuar(ip_olt,user_olt,pass_olt,ena_olt,name_olt)

                def Mac():
                    confirmar_menu = False
                    Tools().clear_pantalla()
                    Tools().banner(hostname)
                    print("%s[+] Comenzando MAC POR MODELO %s" % (cyan,reset_color))
                    if Olt().conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt) == "ERROR":
                            Olt().error_conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)     
                    else:
                            Opciones().mac_list(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
                            Tools().continuar(ip_olt,user_olt,pass_olt,ena_olt,name_olt)

                def Download_fail():
                    confirmar_menu = False
                    Tools().clear_pantalla()
                    Tools().banner(hostname)
                    print("%s[+] Comenzando RESET POR DONWLOAD FAIL %s" % (cyan,reset_color))
                    if Olt().conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt) == "ERROR":
                            Olt().error_conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)     
                    else:
                            Opciones().download_fail(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
                            Tools().continuar(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
                
                def Backup_OLT():
                    confirmar_menu = False
                    Tools().clear_pantalla()
                    Tools().banner(hostname)
                    print("%s[+] Comenzando BACKUP de OLT %s" % (cyan,reset_color))
                    if Olt().conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt) == "ERROR":
                            Olt().error_conexion_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)     
                    else:
                            Opciones().bkp_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
                            Tools().continuar(ip_olt,user_olt,pass_olt,ena_olt,name_olt)

                def Volver():
                    confirmar_menu = False
                    Tools().clear_pantalla()
                    Tools().banner(hostname)
                    Main().main(hostname)

                def error():
                    print("%s[-] Esa opcion no esta en el menu, intente de nuevo %s" % (amarillo,reset_color))
                sw = {
                    0: Volver,
                    1: Limpieza,
                    2: Modelos,
                    3: Total,
                    4: Reset,
                    5: Firmware,
                    6: Rango,
                    7: Descripcion,
                    8: Sin_perfil,
                    9: Actualizar_firmware,
                    10: Reiniciar_plan,
                    11: Sin_ip,
                    12: Check_firmware,
                    13: Mac,
                    14: Download_fail,
                    15: Backup_OLT
                }
                opcion = input("\n%s[*] Seleccione una opcion (quit para salir): %s" % (cyan,reset_color))
                if opcion == "quit":
                        confirmar_menu = False
                        print("%s[*] Muchas gracias por usar el script!%s" % (magenta,reset_color))
                        Tools().borrar_archivos()
                        sys.exit(1)       
                elif opcion == "" or opcion.isalpha() == True:
                    error()
                else:
                    opcion = int(opcion)
                    sw.get(opcion,error)()   
            except KeyboardInterrupt:
                    Tools().clear_pantalla()
                    print("%s\n[-]Detectamos interrupcion por teclado (ctrl+c).Empezando nuevamente ..." % rojo)
                    Tools().menu_again(hostname)

##### TOOLS #######

class Tools():

    def install(self):
        from importlib import import_module
        packages = ["pexpect","os","time","sys","subprocess","ipaddress",]
        print("\n%s[*] Chequeando requisitos:\n" % cyan)
        for package in packages:
            try: 
                lib = import_module(package)
                print("%s[+] Requisito %s .....%sOK"% (blanco,package.ljust(10),verde))
            except: 
                import subprocess
                import sys
                try:
                    ping_check = subprocess.call(["ping", "-c", "1", "-n", "-W", "1", "google.com"],stdout=subprocess.DEVNULL,stderr=subprocess.DEVNULL)
                    if ping_check != 0 :
                        print("%s[-] No se pudo instalar %s%s%s" % (rojo,amarillo,package,reset_color))
                        return "Sin salida"
                        break
                    else:
                        subprocess.check_call([sys.executable, "-m", "pip", "install", package],stdout=subprocess.DEVNULL,stderr=subprocess.DEVNULL)
                        print("%s[+] Requisito %s  Instalado" % (verde,package.ljust(10)))
                except:
                    print("Este requisito no se pudo instalar >>>> %s " % package)


    def banner(self,hostname):
        print(chr(27) + "[2J")
        print("""%s
         ██████╗ ██████╗  ██████╗██╗ ██████╗ ███╗   ██╗███████╗███████╗     ██████╗ ███╗   ██╗████████╗
        ██╔═══██╗██╔══██╗██╔════╝██║██╔═══██╗████╗  ██║██╔════╝██╔════╝    ██╔═══██╗████╗  ██║╚══██╔══╝
        ██║   ██║██████╔╝██║     ██║██║   ██║██╔██╗ ██║█████╗  ███████╗    ██║   ██║██╔██╗ ██║   ██║   
        ██║   ██║██╔═══╝ ██║     ██║██║   ██║██║╚██╗██║██╔══╝  ╚════██║    ██║   ██║██║╚██╗██║   ██║   
        ╚██████╔╝██║     ╚██████╗██║╚██████╔╝██║ ╚████║███████╗███████║    ╚██████╔╝██║ ╚████║   ██║   
         ╚═════╝ ╚═╝      ╚═════╝╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝╚══════╝     ╚═════╝ ╚═╝  ╚═══╝   ╚═╝ 
                    %s| by Emilio Airut. | version 1.10 | last update: %s |                         
                            %s""" % (verde,azul,fecha_actualizacion,reset_color))                    

        print("%s[+] Estas en %s\n" % (verde,hostname))


    def clear_pantalla(self):
        if (os.name in ('ce', 'nt', 'dos')):
            os.system('cls')
        elif ('posix' in os.name):
            os.system('clear')


    def menu_again(self,hostname):
            Tools().banner(hostname)
            Main().main(hostname)        


    def borrar_archivos(self):
            archivos = ["/tmp/expect-olt.txt","/tmp/placas_olt.txt","/tmp/inactive_ont.txt","/tmp/ip_olt_0.txt","/tmp/ip_olt.txt","/tmp/usuario_olt.txt","/tmp/password_olt.txt","/tmp/enable_password_olt.txt",
            "/tmp/enable_password_olt.txt","/tmp/name_olt.txt","/tmp/acceso_olt.txt","/tmp/user_elegido.txt","/tmp/pass_elegido.txt","/tmp/name_elegido.txt","/tmp/enable_elegido.txt","/tmp/sacamos_inactivas.txt","/tmp/sacamos_inactivas2.txt","/tmp/ID.txt","/tmp/id.txt",
            "/tmp/ont_models.txt","/tmp/modelos_ont.txt","gp.txt","reset.txt","ont_reset.txt","GP.txt","models_ont.txt","total_ont.txt","/tmp/ont_sin_perfil.txt","/tmp/ont_sin_perfil.txt","/tmp/ont_sin_ip.txt"]

            for archivo in archivos:
                    if os.path.exists(archivo):
                            os.remove(archivo)


    def continuar(self,ip_olt,user_olt,pass_olt,ena_olt,name_olt):
        os.system("bash -c 'read -s -n 1 -p \"\n%s[*] Presione una tecla para continuar...%s\"'" % (blanco,reset_color))
        Tools().clear_pantalla()                                        
        Tools().banner(hostname)
        Main().menu(hostname,ip_olt,user_olt,pass_olt,ena_olt,name_olt)    

########### OLT ###############

class Olt():
    def conexion_olt(self,ip_olt,user_olt,pass_olt,ena_olt,name_olt):
            try:    
                    print("%s[+] Accediendo a OLT %s %s%s" % (verde,ip_olt,name_olt,reset_color))
                    os.system(">/tmp/expect-olt.txt")
                    telconn = pexpect.spawn('telnet {}'.format(ip_olt))
                    telconn.logfile = open ( '/tmp/expect-olt.txt' , 'ab' )
                    telconn.expect( ".* login:" )
                    telconn.send( "%s" % user_olt + "\r" )
                    telconn.expect("Password:")
                    telconn.send( "{}".format(pass_olt) + "\r")
                    telconn.expect(".*>")
                    telconn.send("ena" + "\r")
                    telconn.expect("Password:")
                    telconn.send( "%s" % ena_olt + "\r" )
                    telconn.expect(".#")
                    telconn.send("show onu info| redirect results.txt" + "\r")
                    telconn.expect(".*#")
                    telconn.send( "con t" + "\r" )
                    telconn.expect(".#")
                    telconn.send( "do q sh -l" + "\r")
                    telconn.expect(".#")
                    telconn.send( "cat /etc/results.txt" + "\r")
                    telconn.expect(".#")
                    telconn.send( "exit" + "\r" )
                    telconn.expect(".#")
                    telconn.send( "exit" + "\r" )
                    telconn.expect(".#")
                    telconn.send( "show system | include Model" + "\r" )
                    telconn.expect(".#")
                    telconn.send( "exit" + "\r" )
                    modelo_olt = os.popen("cat /tmp/expect-olt.txt | grep Name | cut -f 2 -d: | sed 's/ //'").read().strip()
                    print("%s[+] OLT modelo %s" % (verde,modelo_olt))
                    #return modelo_olt
            except:
                    return "ERROR"


    def error_conexion_olt(self,ip_olt,user_olt,pass_olt,ena_olt,name_olt):
            Tools().clear_pantalla()
            Tools().banner(hostname)
            print("\n%s[-]  >>>>>>>>>>>>>>>>>>>>>> ERROR DE CONEXION A OLT %s <<<<<<<<<<<<<<<<<<<<<<<<<" % (rojo,ip_olt))
            print("     POR FAVOR VERIFICAR IP Y/O CREDENCIALES CORRESPONDE A LA OLT SELECCIONADA%s\n" % blanco)
            Tools().continuar(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
            # os.system("bash -c 'read -s -n 1 -p \"%s[*] Presione una tecla para continuar...%s\"'" % (verde,reset_color))
            # Tools().clear_pantalla()
            # Tools().menu_again(hostname)

    def modelo_olt(self):
        modelo_olt = os.popen("cat /tmp/expect-olt.txt | grep Name | cut -f 2 -d: | sed 's/ //'").read().strip()
        return modelo_olt


    def pre_placas_olt(self,ip_olt,user_olt,pass_olt,ena_olt,name_olt):
        modelo_olt = os.popen("cat /tmp/expect-olt.txt | grep Name | cut -f 2 -d: | sed 's/ //'").read().strip()
        list_placa_olt = []
        familia_olt1 = "FK-OLT-G2500_SFU","FK-OLT-G1040_SFU","FK-OLT-G8S","FK-OLT-G4S","LD3008","LD3016"
        familia_olt2 = "LD3032"
        if modelo_olt in familia_olt1:
            placa_olt = os.popen("cat /tmp/expect-olt.txt | grep '|' | egrep -v '(OLT|show|More)'| awk '{{print $1}}' | awk '!x[$0]++'").read().splitlines()
        elif modelo_olt in familia_olt2:
            placa_olt = os.popen("cat /tmp/expect-olt.txt | grep '|' | egrep -v '(OLT|show|More)'| awk '{print $1}' | awk '!x[$0]++' | awk -F 'GPON' '{print $2}'").read().splitlines()
        list_placa_olt = [list_placa_olt + [placa] for placa in placa_olt]
        list_placa_olt = [y for x in list_placa_olt for y in x]
        # for placa in placa_olt:
        #     list_placa_olt.append(placa)
        placas_a_recorrer = "  ".join(z for z in list_placa_olt)
        print("%s[+] Se deben recorrer las placas/puerto:\n\n%s{}\n".format(placas_a_recorrer) % (verde,blanco))
        Olt().placas_olt(ip_olt,user_olt,pass_olt,ena_olt,list_placa_olt,modelo_olt)
        list_modelos_ont = []
        modelos = os.popen("cat /tmp/ont_models.txt |grep '|' | grep -vE '(Model|R\)|OLT)' | awk '{print $5}' | awk '!x[$0]++' | sort | tr '\015' '\n' | sed '/^ *$/d' | grep -vw '-'").read().splitlines()
        for modelo in modelos:
            list_modelos_ont.append(modelo)            
        return list_placa_olt,modelo_olt,list_modelos_ont


    def placas_olt(self,ip_olt,user_olt,pass_olt,ena_olt,list_placa_olt,modelo_olt):
            os.system(">/tmp/ont_models.txt")
            familia_olt1 = "FK-OLT-G2500_SFU","FK-OLT-G1040_SFU","FK-OLT-G8S","FK-OLT-G4S","LD3008","LD3016"
            familia_olt2 = "LD3032"
            telconn = pexpect.spawn('telnet {}'.format(ip_olt))
            telconn.expect( ".* login:" )
            telconn.send("{}".format(user_olt) + "\r")
            telconn.expect("Password:")
            telconn.send( "{}".format(pass_olt) + "\r")
            telconn.expect(".*>")
            telconn.send("ena" + "\r")
            telconn.expect("Password:")
            telconn.send( "{}".format(ena_olt) + "\r")
            telconn.expect(".*#")
            telconn.send("terminal length 0" + "\r")
            telconn.expect(".*#")
            print("%s[+] Aguarde ... Extrayendo informacion de ONTS %s" % (verde,reset_color))
            for GPON in list_placa_olt:
                if modelo_olt in familia_olt1:
                    telconn.logfile = open ('/tmp/ont_models.txt' , 'ab' )
                    telconn.send( "show onu model-name {}".format(GPON) + "\r")
                    telconn.expect(".*#")
                    telconn.send( "show onu firmware version {}".format(GPON) + "\r")
                    telconn.expect(".*#")
                elif modelo_olt in familia_olt2:
                    telconn.logfile = open ('/tmp/ont_models.txt' , 'ab' )
                    telconn.send( "show onu model-name gpon {}".format(GPON) + "\r")
                    telconn.expect(".*#")
                #print("%s[+] Extrayendo informacion de ONT en placa {}%s".format(GPON) % (verde,reset_color))


    def modelos_ont(self,ip_olt,list_modelos_ont):
        Tools().clear_pantalla()
        Tools().banner(hostname)
        print("\n%s[+] Estos son los modelos de ONT encontrados en la OLT %s :\n%s" % (cyan,ip_olt,blanco))
        opcion= 1
        dicc_modelos={}
        for modelo in list_modelos_ont:
                modelo = modelo.rstrip()
                cantidad_modelo = os.popen("cat /tmp/ont_models.txt |grep '|' | grep -v Model | awk '{print $5}' | grep %s | sort | tr '\015' '\n' | sed '/^ *$/d' | wc -l" % (modelo)).read().strip()
                print("{}){} ({})".format(opcion,modelo,cantidad_modelo))
                dicc_modelos["{}".format(opcion)] = "{}".format(modelo)
        #funciona >> list_modelos.update({"{}".format(opcion):"{}".format(modelo)})
                opcion+= 1
        list_modelos2 = dicc_modelos.keys()
        list_modelos3 = []
        for ip in list_modelos2:
                list_modelos3.append(ip)    
        return dicc_modelos,list_modelos3



    def recorrer_placas(self,ip_olt,user_olt,pass_olt,ena_olt,list_placa_olt,ont_elegida,parametro,modelo_olt):
            familia_olt1 = "FK-OLT-G2500_SFU","FK-OLT-G1040_SFU","FK-OLT-G8S","FK-OLT-G4S","LD3008","LD3016"
            familia_olt2 = "LD3032"
            telconn = pexpect.spawn('telnet {}'.format(ip_olt))
            telconn.timeout=300
            telconn.expect( ".* login:" )
            telconn.send("{}".format(user_olt) + "\r")
            telconn.expect("Password:")
            telconn.send( "{}".format(pass_olt) + "\r")
            telconn.expect(".*>")
            telconn.send("ena" + "\r")             
            telconn.expect("Password:")
            telconn.send( "{}".format(ena_olt) + "\r")
            telconn.expect(".*#")
            telconn.send("terminal length 0" + "\r")
            telconn.expect(".*#")
            telconn.send("conf t" + "\r")
            telconn.expect(".*#")
            validar_sin_prolile = []
            ont_reset_list = []
            for GPON in list_placa_olt:
                if modelo_olt in familia_olt1:
                    telconn.logfile = open ('/tmp/sacamos_inactivas.txt' , 'ab' )
                    telconn.send("gp" + "\r")
                    telconn.expect(".*#")                
                    telconn.send("gpon {}".format(GPON) + "\r")
                    telconn.expect(".*#")                
                elif modelo_olt in familia_olt2:
                    telconn.logfile = open ('/tmp/sacamos_inactivas.txt' , 'ab' )
                    telconn.send("interface gpon {}".format(GPON) + "\r")
                    telconn.expect(".*#")
                
                if parametro == "limpiar_ont":
                        validar_sin_prolile = "True"
                        ont_reset = os.popen("cat /tmp/inactive_ont.txt | grep '{}' | awk '{{print $2}}'".format(GPON)).read().splitlines()
                        lista_ont = []
                        for id_ont in ont_reset:
                           if id_ont and id_ont not in lista_ont:
                                lista_ont.append(id_ont)
                                limpiar_ont_ids = ",".join(lista_ont)                
                        if lista_ont:
                            telconn.send("no onu {}".format(limpiar_ont_ids) + "\r")
                            telconn.expect(".*#")
                            print("%s[+] Limpiando ONT inactivas en GPON {}%s".format(GPON) % (verde,reset_color))
                elif parametro == "reset_ont":
                        validar_sin_prolile = "True"
                        if ont_elegida == "FK-ONT-G420W/ACS2":
                            ont_reset = os.popen("cat /tmp/ont_models.txt | grep -w '{}' | awk '{{print $1,$3}}' | grep '^{}' | awk '{{print $2}}'".format(ont_elegida,GPON)).read().splitlines()
                        elif ont_elegida == "ont_donwload_fail":
                            ont_reset = os.popen("cat /tmp/ont_models.txt | grep -i fail | awk '{{print $1,$3}}' | grep '^{}' | awk '{{print $2}}'".format(GPON)).read().splitlines()
                        else:
                            ont_reset = os.popen("cat /tmp/ont_models.txt | grep -w '{}' | awk '{{print $1,$3,$5}}' | grep '^{}' | grep -v /ACS2 | awk '{{print $2}}'".format(ont_elegida,GPON)).read().splitlines()
                        lista_ont = []
                        for id_ont in ont_reset:
                            if id_ont not in lista_ont:
                                lista_ont.append(id_ont)                
                                ont_reset_list.append(id_ont)                
                        reset_ont2 = ",".join(lista_ont)
                        if lista_ont:
                            telconn.send("onu reset {}".format(reset_ont2) + "\r")
                            telconn.expect(".*#")
                            print("%s[+] Reiniciando ONT en GPON {}%s".format(GPON) % (verde,reset_color))

                elif parametro == "firmware_ont":
                        validar_sin_prolile = "True"
                        #print("%s[+] Chequeando firmware de ONT en GPON {}%s".format(GPON) % (verde,reset_color))
                        if ont_elegida == "FK-ONT-G420W/ACS2":
                            ont_reset = os.popen("cat /tmp/ont_models.txt | grep -w '{}' | awk '{{print $1,$3}}' | grep '^{}' | awk '{{print $2}}'".format(ont_elegida,GPON)).read().splitlines()
                        else:
                            ont_reset = os.popen("cat /tmp/ont_models.txt | grep -w '{}' | awk '{{print $1,$3,$5}}' | grep '^{}' | grep -v /ACS2 | awk '{{print $2}}'".format(ont_elegida,GPON)).read().splitlines()
                        lista_ont = []
                        for id_ont in ont_reset:
                            if id_ont not in lista_ont:
                                lista_ont.append(id_ont)
                        firmware_ont2 = ",".join(lista_ont)
                        if lista_ont:
                            telconn.send("show onu firmware version {}".format(firmware_ont2) + "\r")
                            telconn.expect(".*#")

                elif parametro == "check_firmware_ont":
                        validar_sin_prolile = "True"
                        #print("%s[+] Chequeando firmware de ONT en GPON {}%s".format(GPON) % (verde,reset_color))
                        id_ont = os.popen("cat /tmp/expect-olt.txt | grep '{}' | awk '{{print $3}}'".format(ont_elegida)).read()
                        #id_ont = id_ont.splitlines()
                        telconn.send("show onu firmware version {}".format(id_ont) + "\r")
                        telconn.expect(".*#")
                            
                elif parametro == "descripcion_ont":
                        validar_sin_prolile = "True"
                        if ont_elegida == "FK-ONT-G420W/ACS2":
                            ont_reset = os.popen("cat /tmp/ont_models.txt | grep -w '{}' | awk '{{print $1,$3}}' | grep '^{}' | awk '{{print $2}}'".format(ont_elegida,GPON)).read().splitlines()
                        else:
                            ont_reset = os.popen("cat /tmp/ont_models.txt | grep -w '{}' | awk '{{print $1,$3,$5}}' | grep '^{}' | grep -v /ACS2 | awk '{{print $2}}'".format(ont_elegida,GPON)).read().splitlines()
                        lista_ont = []
                        for id_ont in ont_reset:
                           if id_ont not in lista_ont:
                                lista_ont.append(id_ont)
                        descripcion_ont = ",".join(lista_ont)
                        if lista_ont:
                            telconn.send("show onu description {}".format(descripcion_ont) + "\r")
                            telconn.expect(".*#")
                        print("%s[+] Obteniendo descripcion de ONT en GPON {}%s".format(GPON) % (verde,blanco))

                elif parametro == "ont_sin_profile":
                        id_ont = os.popen("cat /tmp/ont_sin_perfil.txt | grep '{}' | awk '{{print $2}}'".format(GPON)).read().splitlines()
                        lista_ont = []
                        for ids in id_ont:
                                if ids not in lista_ont:
                                        lista_ont.append(ids)
                        ont_sin_profile2 = ",".join(lista_ont)    
                        if ont_sin_profile2:
                                validar_sin_prolile.append(ont_sin_profile2)
                                print("%s[*] Encontramos los id de ont sin perfil: %s en %s, ahora reiniciamos%s" % (amarillo,ont_sin_profile2,GPON,reset_color))                                
                                telconn.send("no onu {}".format(ont_sin_profile2) + "\r")
                                telconn.expect(".*#")
                        #else:
                        #    print("%s[+] No hay ONT sin perfil en %s" % (verde,GPON))

                elif parametro == "factory_reset":
                        #print("\n%s[*] Chequeo IP de gestion de ONT en GPON {} aguarde... %s".format(GPON) % (amarillo,reset_color))
                        os.system(">/tmp/sacamos_inactivas.txt")
                        ont_reset = os.popen("cat /tmp/ont_models.txt | grep \"LD420-10R\|LD421-21W\" | grep {} | awk '{{print $3}}'".format(GPON)).read().splitlines()
                        lista_ont = []
                        if ont_reset:
                            for id_ont in ont_reset:
                                if id_ont and id_ont not in lista_ont :
                                    lista_ont.append(id_ont)
                        if lista_ont:   
                            ont_sin_ip = []
                            for id_ont in lista_ont:    
                                telconn.logfile = open ( '/tmp/sacamos_inactivas.txt' , 'ab' )
                                telconn.send("show onu ip {} | grep -A4 1(0x000".format(id_ont) + "\r")
                                telconn.expect(".*#")
                                sin_ip = os.popen('cat /tmp/sacamos_inactivas.txt | grep -A4 "ONU : %s"' % id_ont).read()
                                if "0.0.0.0" in sin_ip:
                                    print("%s[-] SIN IP DE GESTION DE ONT {} EN GPON {} %s".format(id_ont,GPON) % (rojo,reset_color))
                                    ont_sin_ip.append(id_ont)
                                #else:
                        
                            factory_reset2 = ",".join(ont_sin_ip)
                            if ont_sin_ip:
                                with open ("/tmp/ont_sin_ip.txt","a") as ont:
                                    ont.write("\n[+] Estos son los factory reset de las ONT en %s : %s " % (GPON,factory_reset2))                    
                                    print("%s[+] Realizamos FACTORY RESET de ONT %s sin ip de gestion en %s " % (rojo,factory_reset2,GPON))
                                    telconn.send("onu restore-factory reset {}".format(factory_reset2) + "\r")
                                    telconn.expect(".*#")                            
                            else:
                                print("%s[+] Todas las ONT LD420-10R tienen IP en %s" % (verde,GPON))

                elif parametro == "info":
                        validar_sin_prolile = "True"
                        ont_info = os.popen("cat /tmp/ont_models.txt | grep -w '{}' | awk '{{print $1,$3}}' | grep '^{}' | awk '{{print $2}}'".format(ont_elegida,GPON)).read().splitlines()
                        lista_ont = []
                        for id_ont in ont_info:
                            if id_ont not in lista_ont:
                                lista_ont.append(id_ont)                
                        reset_ont2 = ",".join(lista_ont)
                        if lista_ont:
                            telconn.send("show onu info {}".format(reset_ont2) + "\r")
                            telconn.expect(".*#")
                            #print("%s[+] INFO ONT en GPON {}%s".format(GPON) % (verde,reset_color))


            if ont_reset_list:
                print("\n%s[+] Se reiniciaron %s ont modelo %s en total " % (verde,len(ont_reset_list),ont_elegida))

            try:
                if not validar_sin_prolile:
                    Tools().clear_pantalla()
                    Tools().banner(hostname)
                    print("%s\n[+] No hay ONT sin perfil. Felicidades!!" % verde)         
            except:
                pass


    def placas_olt_download_fail(self,ip_olt,user_olt,pass_olt,ena_olt,list_placa_olt,modelo_olt):
            os.system(">/tmp/ont_models.txt")
            familia_olt1 = "FK-OLT-G2500_SFU","FK-OLT-G1040_SFU","FK-OLT-G8S","FK-OLT-G4S","LD3008","LD3016"
            familia_olt2 = "LD3032"
            telconn = pexpect.spawn('telnet {}'.format(ip_olt))
            telconn.expect( ".* login:" )
            telconn.send("{}".format(user_olt) + "\r")
            telconn.expect("Password:")
            telconn.send( "{}".format(pass_olt) + "\r")
            telconn.expect(".*>")
            telconn.send("ena" + "\r")
            telconn.expect("Password:")
            telconn.send( "{}".format(ena_olt) + "\r")
            telconn.expect(".*#")
            telconn.send("terminal length 0" + "\r")
            telconn.expect(".*#")
            print("%s[+] Aguarde ... Extrayendo informacion de ONTS %s" % (verde,reset_color))
            for GPON in list_placa_olt:
                if modelo_olt in familia_olt1:
                    telconn.logfile = open ('/tmp/ont_models.txt' , 'ab' )
                    telconn.send( "show onu firmware version {}".format(GPON) + "\r")
                    telconn.expect(".*#")
                elif modelo_olt in familia_olt2:
                    telconn.logfile = open ('/tmp/ont_models.txt' , 'ab' )
                    telconn.send( "show onu firmware version gpon {}".format(GPON) + "\r")
                    telconn.expect(".*#")
                #print("%s[+] Extrayendo informacion de ONT en placa {}%s".format(GPON) % (verde,reset_color))                      


    def gpon_olt(self,ip_olt,user_olt,pass_olt,ena_olt):
            telconn = pexpect.spawn('telnet {}'.format(ip_olt))
            telconn.logfile = open ( '/tmp/ont_sin_perfil.txt' , 'wb' )
            telconn.expect( ".* login:" )
            telconn.send("{}".format(user_olt) + "\r")
            telconn.expect("Password:")
            telconn.send( "{}".format(pass_olt) + "\r")
            telconn.expect(".*>")
            telconn.send("ena" + "\r")
            telconn.expect("Password:")
            telconn.send( "{}".format(ena_olt) + "\r")
            telconn.expect(".*#")
            telconn.send("show run gpon" + "\r")
            telconn.expect("--More--")
            telconn.send("q")
            telconn.expect(".*#")
            telconn.send("exit" + "\r")


    def ftp_olt_upgrade_ont(self,ip_olt,user_olt,pass_olt,ena_olt,GPON,check_firmware_ont,firm_nuevo):
        ip_ftp = os.popen("cat /tmp/ont_sin_perfil.txt | awk -F 'ftp' '{{print $2}}' | sed '/^ *$/d'| uniq | awk '{{print $1}}'").read().rstrip()
        user_ftp = os.popen("cat /tmp/ont_sin_perfil.txt | awk -F 'ftp' '{{print $2}}' | sed '/^ *$/d'| uniq | awk '{{print $2}}'").read().rstrip()
        pass_ftp = os.popen("cat /tmp/ont_sin_perfil.txt | awk -F 'ftp' '{{print $2}}' | sed '/^ *$/d'| uniq | awk '{{print $3}}'").read().rstrip()
        telconn = pexpect.spawn('telnet {}'.format(ip_olt))
        telconn.timeout=30
        telconn.expect( ".* login:" )
        telconn.send("{}".format(user_olt) + "\r")
        telconn.expect("Password:")
        telconn.send( "{}".format(pass_olt) + "\r")
        telconn.expect(".*>")
        telconn.send("ena" + "\r")             
        telconn.expect("Password:")
        telconn.send( "{}".format(ena_olt) + "\r")
        telconn.expect(".*#")
        telconn.send("conf t" + "\r")
        telconn.expect(".*#")
        telconn.send("gp" + "\r")
        telconn.expect(".*#")
        telconn.send("gpon-olt {}".format(GPON) + "\r")
        telconn.expect(".*#")
        telconn.send("onu upgrade %s %s ftp %s %s %s " % (check_firmware_ont,firm_nuevo,ip_ftp,user_ftp,pass_ftp) + "\r")
        telconn.expect(".*#")
        telconn.send("end" + "\r")
        telconn.expect(".*#")
        telconn.send( "exit" + "\r")
        print("%s[+] Finalizamos la carga de firmware. Para ver el proceso puedes usar la opcion 9 del menu principal nuevamente" % verde)    

####### OPCIONES ########

class Opciones():
    #1
    def limpiar_inactivas(self,ip_olt,user_olt,pass_olt,ena_olt,name_olt):
            familia_olt1 = "FK-OLT-G2500_SFU","FK-OLT-G1040_SFU","FK-OLT-G8S","FK-OLT-G4S","LD3008","LD3016"
            familia_olt2 = "LD3032"
            def limpiar_ont_inactivas(list_placa_olt,modelo_olt):
                ont_elegida = ""
                parametro = "limpiar_ont"
                Olt().recorrer_placas(ip_olt,user_olt,pass_olt,ena_olt,list_placa_olt,ont_elegida,parametro,modelo_olt)
                Tools().clear_pantalla()
                Tools().banner(hostname)
                print("\n%s[+] Limpieza finalizada correctamente! %s" % (verde,reset_color))
            #try:
            list_placa_olt,modelo_olt,list_modelos_ont = Olt().pre_placas_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
            os.system("cat /tmp/expect-olt.txt | grep -w Inactive | awk '{print $1,$3,$7}' > /tmp/inactive_ont.txt")
            if modelo_olt in familia_olt1: 
                inactivas = os.popen("cat /tmp/expect-olt.txt | grep -w Inactive | awk '{print $1,$3,$7}'").read().splitlines()
            elif modelo_olt in familia_olt2:
                inactivas = os.popen("cat /tmp/expect-olt.txt | grep -w Inactive | awk -F 'GPON' '{print $2}' | awk '{print $1,$3,$7}'").read().splitlines()
            #inactivas_total = len(inactivas)
            lista_inactivas = []
            for inactiva in inactivas:
                inactiva = inactiva.split(" ")
                lista_inactivas.append(inactiva)
            lista_inactiva = []
            for i in range(len(lista_inactivas)):
                lista_inactiva.append([lista_inactivas[i][0], lista_inactivas[i][1],lista_inactivas[i][2]])
            #print(inactiva)
            if lista_inactivas:
                    # print("%s[+] Estas son las ONT inactivas:%s\n" % (cyan,blanco))
                    # print("%splaca\tID\t    MAC" % blanco)
                    # for ont_inactiva in lista_inactiva:
                    #     inactiva = ont_inactiva[0] + "\t" + ont_inactiva[1] + "\t" + ont_inactiva[2]
                    #     print(inactiva)
                    # print("\nTOTAL_INACTIVAS: %s" % len(lista_inactivas))
                    Tools().clear_pantalla()
                    Tools().banner(hostname)
                    print("%s[+] Comenzando Limpieza ..." % verde)
                    limpiar_ont_inactivas(list_placa_olt,modelo_olt)                        
                    # confirmar = True
                    # while confirmar:
                    #         continuar = str(input("%s[+] Desea continuar con la limpieza de ONT inactivas (s/n): %s" % (amarillo,reset_color)))
                    #         continuar = continuar.lower()
                    #         if continuar == "s":
                    #                 confirmar = False
                    #                 print("%s[+] Comenzando Limpieza ..." % verde)
                    #                 limpiar_ont_inactivas(list_placa_olt)
                    #         elif continuar == "n":
                    #                 confirmar = False
                    #                 Tools().clear_pantalla()
                    #                 Tools().banner(hostname)
                    #                 print("%s[-] Abortando limpieza por usuario %s" % (magenta,reset_color))
                    #                 return "TERMINAR"
                    #         else:
                    #                print("%s[-] La opcion disponible es 's' o 'n'%s" % (amarillo,reset_color))
            else:
                Tools().clear_pantalla()
                Tools().banner(hostname)
                print("%s[+] No hay ONT inactivas en la OLT con ip {}%s".format(ip_olt) % (verde,reset_color))
            #except:
            #    print("\n%s[-]>>>>>>>>>> ERROR DE LIMPIEZA  <<<<<<<<<<<<<\n" % rojo )

    #2
    def ver_modelos(self,ip_olt,user_olt,pass_olt,ena_olt,name_olt):

            try:
                list_placa_olt,modelo_olt,list_modelos_ont = Olt().pre_placas_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
                Tools().clear_pantalla()
                Tools().banner(hostname)
                if list_modelos_ont:
                    print("\n%s[+] opcion 2 Estos son los modelos de ONT encontrados en la OLT %s %s\n%s" % (cyan,ip_olt,modelo_olt,blanco))
                    opcion= 1
                    for modelo in list_modelos_ont:
                        if modelo == "FK-ONT-G420W/ACS2":
                            cantidad_modelo = os.popen("cat /tmp/ont_models.txt |grep '|' | grep -vE '(Model|\(R\)|OLT)' | grep '/ACS2' | awk '{print $5}' | grep -vw '-' | grep %s | sort | tr '\015' '\n' | sed '/^ *$/d' | wc -l" % (modelo)).read().strip()
                        else:
                            cantidad_modelo = os.popen("cat /tmp/ont_models.txt |grep '|' | grep -vE '(Model|\(R\)|OLT)' | grep -v '/ACS2' | awk '{print $5}' | grep -vw '-'| grep %s | sort | tr '\015' '\n' | sed '/^ *$/d' | wc -l" % (modelo)).read().strip()
                        print("{}){} ({})".format(opcion,modelo,cantidad_modelo)) 
                    #funciona >> list_modelos.update({"{}".format(opcion):"{}".format(modelo)})
                        opcion+= 1
                else:
                    print("\n%s[+] No hay ONT activas en la OLT %s :\n%s" % (rojo,ip_olt,blanco))

            except:
                #Tools().borrar_archivos()
                print("\n%s[-]>>>>>>>>>> ERROR DE VER MODELO  <<<<<<<<<<<<<\n" % rojo)

    #3
    def cant_ont(self,ip_olt,user_olt,pass_olt,ena_olt,name_olt):
            try:
                list_placa_olt,modelo_olt,list_modelos_ont = Olt().pre_placas_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
                total = []
                for models in list_modelos_ont:
                    models = models.strip()
                    if models == "FK-ONT-G420W/ACS2":
                        valores = os.popen("cat /tmp/ont_models.txt | grep '|' | grep -v OLT | awk '{{print $1,$5}}' | grep '/ACS2' | grep -v -w '-' | grep -w {} | uniq -c | sort -nr".format(models)).read().splitlines()
                        total_modelo = os.popen("cat /tmp/ont_models.txt | grep '|' | grep -v OLT | awk '{{print $1,$5}}' | grep '/ACS2' | grep -v -w '-' | grep -w {} | uniq -c| awk '{{print $1}}' | paste -sd+ | bc".format(models)).read().splitlines()
                    else:
                        valores = os.popen("cat /tmp/ont_models.txt | grep '|' | grep -v OLT | awk '{{print $1,$5}}' | grep -v '/ACS2'| grep -v -w '-' | grep -w {} | uniq -c | sort -nr".format(models)).read().splitlines()              
                        total_modelo = os.popen("cat /tmp/ont_models.txt | grep '|' | grep -v OLT | awk '{{print $1,$5}}' | grep -v '/ACS2'| grep -v -w '-' | grep -w {} | uniq -c| awk '{{print $1}}' | paste -sd+ | bc".format(models)).read().splitlines()
                    lista_valores = []
                    fuera_de_rango = []                
                    for valor in valores:
                        valor2 = valor.split(" ")
                        lista_valores.append(valor2)
                    for indx in range(len(lista_valores)):
                        fuera_de_rango.append([lista_valores[indx][-3], lista_valores[indx][-2],lista_valores[indx][-1]])
                    if fuera_de_rango:
                        print('\n%s[+] ONTS modelo {} ACTIVAS en OLT con ip {}:%s'.format(models,ip_olt) % (cyan,blanco))
                        print("%s    CANTIDAD   placa      MODELO" % blanco)
                        for valor_final in fuera_de_rango:
                            dbm_ont = "\t" + valor_final[0] + "\t" + valor_final[1] +"\t"+ valor_final[2]
                            print(dbm_ont)
                    total.append([models]+[total_modelo])
                print("\n%s[+] Resumen de ONT total\n" % verde)
                for i in total:
                    print("%s{}: {}".format(i[0].ljust(18),i[1][0]) % blanco)
                print(verde)
                total_ont = os.popen('cat /tmp/ont_models.txt |egrep -v "(exit|closed|ena|:|@|model-name|OLT|>|\(D\))" | grep "|" | wc -l').read()
                print("%s[+] Total General OLT %s: %s" % (verde,ip_olt,total_ont))
            except:
                print("\n%s[-]>>>>>>>>>> ERROR DE CANTIDAD <<<<<<<<<<<<<\n" % rojo )

    #4
    def reset_ont(self,ip_olt,user_olt,pass_olt,ena_olt,name_olt):
            list_placa_olt,modelo_olt,list_modelos_ont = Olt().pre_placas_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
            dicc_modelos,list_modelos3 = Olt().modelos_ont(ip_olt,list_modelos_ont)
            try:
                    validar_opcion = True
                    while validar_opcion:
                            eleccion = input("\n%s[*] Por favor seleccione una opcion (quit para salir): %s" % (cyan,reset_color))
                            if eleccion == "quit":
                                    validar_opcion = False
                                    Tools().clear_pantalla()
                                    Tools().banner(hostname)
                                    print("%s[-] Abortando por usuario...%s" % (magenta,reset_color))
                                    Tools().borrar_archivos()
                                    Main().menu(hostname,ip_olt,user_olt,pass_olt,ena_olt,name_olt)                                      
                                    #Tools().menu_again(hostname)
                            elif eleccion in list_modelos3:
                                    validar_opcion = False
                                    ont_elegida = dicc_modelos["{}".format(eleccion)]
                                    os.system(">/tmp/sacamos_inactivas.txt")
                            else:
                                    print("%s[-] Esa no es una opcion del menu, intente de nuevo%s" % (amarillo,reset_color))
            except KeyboardInterrupt:
                    Tools().clear_pantalla()
                    print("%s\n[-]Detectamos interrupcion por teclado (ctrl+c).Empezando nuevamente..." % rojo)
                    Tools().menu_again(hostname)    
            parametro = "reset_ont"
            modelo_olt = Olt().modelo_olt()
            print("%s[+] Comenzando Reset ..." % (verde) )        
            Olt().recorrer_placas(ip_olt,user_olt,pass_olt,ena_olt,list_placa_olt,ont_elegida,parametro,modelo_olt)
            #recorrer_placas(ip_olt,user_olt,pass_olt,ena_olt,list_placa_olt,limpiar_ont,reset_ont,firmware_ont,descripcion_ont,ont_sin_profile,factory_reset,ont_elegida,check_firmware_ont)
            os.system("cat /tmp/sacamos_inactivas.txt | grep '|' > /tmp/sacamos_inactivas2.txt")
            os.system("sed '/OLT/i------------------------------------------------------------------------------------------------------' /tmp/sacamos_inactivas2.txt")                
            #Tools().borrar_archivos()

    #5
    def firmware_ont(self,ip_olt,user_olt,pass_olt,ena_olt,name_olt):
        list_placa_olt,modelo_olt,list_modelos_ont = Olt().pre_placas_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
        dicc_modelos,list_modelos3 = Olt().modelos_ont(ip_olt,list_modelos_ont)
        try:
            validar_opcion = True
            while validar_opcion:
                    eleccion = input("\n%s[*] Por favor seleccione una opcion (quit para salir): %s" % (cyan,reset_color))
                    if eleccion == "quit":
                            validar_opcion = False
                            Tools().clear_pantalla()
                            Tools().banner(hostname)
                            print("%s[-] Abortando por usuario...%s" % (magenta,reset_color))
                            Tools().borrar_archivos()
                            Main().menu(hostname,ip_olt,user_olt,pass_olt,ena_olt,name_olt)                                      
                    elif eleccion in list_modelos3:
                            validar_opcion = False
                            ont_elegida = dicc_modelos["{}".format(eleccion)]
                            os.system(">/tmp/sacamos_inactivas.txt")
                    else:
                            print("%s[-] Esa no es una opcion del menu, intente de nuevo%s" % (amarillo,reset_color))
        except KeyboardInterrupt:
                Tools().clear_pantalla()
                print("%s\n[-]Detectamos interrupcion por teclado (ctrl+c).Empezando nuevamente..." % rojo)
                Tools().menu_again(hostname)   
        print("%s[+] Comenzando comprobacion, aguarde ..." % verde)
        print("%s[+] Chequeando firmware de ONT %s %s" % (verde,ont_elegida,reset_color))
        parametro = "firmware_ont"
        Olt().recorrer_placas(ip_olt,user_olt,pass_olt,ena_olt,list_placa_olt,ont_elegida,parametro,modelo_olt)
        Tools().clear_pantalla()
        Tools().banner(hostname)
        print("%s[+] Estos son los firmware de ONT {}:\n%s".format(ont_elegida) % (cyan,blanco))
        os.system("cat /tmp/sacamos_inactivas.txt | grep '|' > /tmp/sacamos_inactivas2.txt")
        os.system("sed '/OLT/i------------------------------------------------------------------------------------------------------' /tmp/sacamos_inactivas2.txt")

    #6
    def dbm(self,ip_olt,user_olt,pass_olt,ena_olt,name_olt):
            list_placa_olt,modelo_olt,list_modelos_ont = Olt().pre_placas_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
            lista_valores = []
            fuera_de_rango = []        
            for placa in list_placa_olt:
                valores = os.popen("cat /tmp/expect-olt.txt | grep Active | grep {} |awk '{{print $1,$3,$7,$11,$12}}'".format(placa)).read().splitlines()
                for valor in valores:
                    valor2 = valor.split(" ")
                    lista_valores.append(valor2)
            for indx in range(len(lista_valores)):
                #if float(lista_valores[indx][-1]) < 16.0 or float(lista_valores[indx][-1]) > 27.0:
                if float(lista_valores[indx][-1]) > 27.0:
                    fuera_de_rango.append([lista_valores[indx][0], lista_valores[indx][1],lista_valores[indx][3],lista_valores[indx][-1],lista_valores[indx][2]])
            sorted_list = sorted(fuera_de_rango, key=lambda x: x[3] , reverse=False)
            #lista_ordenada_planes = sorted(lista_planes2, key=lambda x: int(x[-1]))
            cantidad_ont = []
            cantidad_ont_rojas = []
            #Tools().clear_pantalla()
            if fuera_de_rango:
                    print("%s[+] Encontramos potencias fuera de rango:\n"% amarillo)
                    print("%splaca\tID\t dBm\t    MAC" % blanco)
                    for valor_final in sorted_list:
                        if float(valor_final[3]) > 27.0:
                            valor_final[3] = rojo+valor_final[3]+blanco
                            cantidad_ont_rojas.append(valor_final[3])
                        dbm_ont = valor_final[0] + "\t" + valor_final[1] +"\t"+ valor_final[2] + valor_final[3] +"\t"+valor_final[4]
                        print(dbm_ont)
                        cantidad_ont.append(dbm_ont)
                    #print("\n%s[-] Hay %s%s%s ont fuera de rango (-16 dBm a -27 dBm)" % (amarillo,blanco,len(cantidad_ont),amarillo))
                    print("\n%s[-] Hay %s%s%s ont fuera de rango " % (amarillo,blanco,len(cantidad_ont),amarillo))
                    #if cantidad_ont_rojas:
                        #print("[-] De las cuales %s%s%s son por baja potencia. Debes revisar con prioridad!" % (rojo,len(cantidad_ont_rojas),amarillo))
                    print("[*] Para una mejor performance a nivel optico, por favor corriga la potencia de las ont reportadas")
                    #Tools().borrar_archivos()
            else:
                    print("\n%s[+] No hay ont fuera de rango. \x1b[5;92mSU ODN ES OPTIMA!\n\x1b[5;0m" % verde)

    #7
    def description(self,ip_olt,user_olt,pass_olt,ena_olt,name_olt):
            list_placa_olt,modelo_olt,list_modelos_ont = Olt().pre_placas_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
            dicc_modelos,list_modelos3 = Olt().modelos_ont(ip_olt,list_modelos_ont)
            try:
                    validar_opcion = True
                    while validar_opcion:
                            eleccion = input("\n%s[*] Por favor seleccione una opcion (quit para salir): %s" % (cyan,reset_color))
                            if eleccion == "quit":
                                    validar_opcion = False
                                    print("%s[-] Abortando por usuario...%s" % (magenta,reset_color))
                                    Tools().borrar_archivos()
                                    sys.exit(1)
                            elif eleccion in list_modelos3:
                                    validar_opcion = False
                                    ont_elegida = dicc_modelos["{}".format(eleccion)]
                                    os.system(">/tmp/sacamos_inactivas.txt")
                            else:
                                    print("%s[-] Esa no es una opcion del menu, intente de nuevo%s" % (amarillo,reset_color))
            except KeyboardInterrupt:
                    Tools().clear_pantalla()
                    print("%s\n[-]Detectamos interrupcion por teclado (ctrl+c).Empezando nuevamente...\n" % rojo)
                    Tools().menu_again(hostname)      
            parametro = "descripcion_ont"
            print("%s[+] Comenzando extraccion de Descripcion..." %(verde))     
            Olt().recorrer_placas(ip_olt,user_olt,pass_olt,ena_olt,list_placa_olt,ont_elegida,parametro,modelo_olt)
            Tools().clear_pantalla()
            Tools().banner(hostname)
            total_descripcion = os.popen("cat /tmp/sacamos_inactivas.txt | grep -a '|' | grep -va OLT | wc -l").read()
            print("%s[+] Estas son las descripciones de ONT {}:\n%s".format(ont_elegida) % (cyan,blanco))
            os.system("cat /tmp/sacamos_inactivas.txt | grep -a '|' | grep -v -a OLT")
            print("\nTotal de ONT : %s" % total_descripcion)

    #8
    def sin_profile(self,ip_olt,user_olt,pass_olt,ena_olt,name_olt):
            list_placa_olt,modelo_olt,list_modelos_ont = Olt().pre_placas_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
            os.system(">/tmp/ont_sin_perfil.txt")
            os.system("cat /tmp/expect-olt.txt |tr '\015' '\n' | grep Active |awk '{print $1,$3,$7,$15}' | while read a b c d ;do if [ -z $d ];then echo $a $b >> /tmp/ont_sin_perfil.txt;fi;done")
            Tools().clear_pantalla()
            Tools().banner(hostname)
            print("%s[+] Comenzando reinicio de ONT sin perfil... " % verde)
            validar_sin_prolile = []
            ont_elegida = ""
            parametro = "ont_sin_profile"
            Olt().recorrer_placas(ip_olt,user_olt,pass_olt,ena_olt,list_placa_olt,ont_elegida,parametro,modelo_olt)

    #9
    def actualizar_firmware(self,ip_olt,user_olt,pass_olt,ena_olt,name_olt):
        mac_ont = input("%s[*] Escribe los ultimos 6 caracteres de la MAC (quit para ir al menu principal) : %s" % (cyan,reset_color))
        mac_ont = mac_ont.lower()
        if mac_ont == "quit":
            Tools().clear_pantalla()                                        
            Tools().banner(hostname)
            Main().menu(hostname,ip_olt,user_olt,pass_olt,ena_olt,name_olt)    
        elif mac_ont == "":
            print("%s[-] Detectamos que no ingresaste MAC. Intenta de nuevo" % rojo)
            Opciones().actualizar_firmware(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
        elif len(mac_ont) != 6:
            print("%s[-] Debes colocar los ultimos 6 caracteres!" % amarillo)
            Opciones().actualizar_firmware(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
        else:
            GPON = os.popen("cat /tmp/expect-olt.txt | grep %s | awk '{{print $1}}' " %(mac_ont) ).read().strip()
            check_firmware_ont = os.popen("cat /tmp/expect-olt.txt | grep %s | awk '{print $3}'" %(mac_ont)).read().rstrip()
            list_placa_olt,modelo_olt,list_modelos_ont = Olt().pre_placas_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
            list_placa_olt = []
            list_placa_olt.append(GPON)
            # list_placa_olt = os.popen("cat /tmp/expect-olt.txt | grep %s | awk '{print $1}'" % mac_ont ).read()
            # list_placa_olt = list_placa_olt.splitlines()
            ont_elegida = mac_ont
            parametro = "check_firmware_ont"
            # modelo_olt = os.popen("cat /tmp/expect-olt.txt  | grep Name | cut -f 2 -d: | sed 's/ //'").read()
            # modelo_olt = modelo_olt.strip()        
            if GPON:
                print("%s[+] Encontramos ONT con MAC %s .Buscando Firmware..." % (verde,mac_ont))
                os.system("> /tmp/sacamos_inactivas.txt")
                #recorrer_placas(ip_olt,user_olt,pass_olt,ena_olt,list_placa_olt,limpiar_ont,reset_ont,firmware_ont,descripcion_ont,ont_sin_profile,factory_reset,ont_elegida,check_firmware_ont)
                Olt().recorrer_placas(ip_olt,user_olt,pass_olt,ena_olt,list_placa_olt,ont_elegida,parametro,modelo_olt)
                print("%s[*] Este es el firmware actual de la ONT: %s\n" %(cyan,blanco))
                os.system("cat /tmp/sacamos_inactivas.txt | grep '|' | grep '/'")
            else:
                print("%s[-] No encontramos la MAC %s en la OLT, intenta de nuevo" % (rojo,mac_ont))
                Opciones().actualizar_firmware(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
            final = True
            while final:
                confirmar = input("%s\n[*] Quieres actualizarle el firmware? (s/n): %s" % (amarillo,reset_color))
                confirmar = confirmar.lower()
                if confirmar == "s":
                    final = False
                    firm_nuevo = input("%s[*] Cual es el firmware para actualizar ? : %s" % (cyan,reset_color))
                    if firm_nuevo == "":
                        print("%s[-] Detectamos que no ingresaste archivo firmware. Intenta de nuevo" % rojo)
                    else:
                        #final = False    
                        if not os.path.isfile("/home/furukawa/%s" % firm_nuevo):
                            print("%s[-] No encontramos %s en /home/furukawa/ debes colocar el firmware en ese directorio para continuar" % (rojo,firm_nuevo))

                        else:
                            print("%s[+] Firmware encontrado en /home/furukawa/, continuamos..." % verde)
                            print("%s[+] Cargando firmware en ont %s en %s" % (verde,mac_ont,GPON))
                            os.system(">/tmp/ont_sin_perfil.txt")
                            Olt().gpon_olt(ip_olt,user_olt,pass_olt,ena_olt)
                            Olt().ftp_olt_upgrade_ont(ip_olt,user_olt,pass_olt,ena_olt,GPON,check_firmware_ont,firm_nuevo)
                            #Olt().ftp_olt_upgrade_ont(ip_olt,user_olt,pass_olt,ena_olt,GPON,check_firmware_ont,firm_nuevo,ip_ftp,user_ftp,pass_ftp)
                            # ip_ftp = os.popen("cat /tmp/ont_sin_perfil.txt | awk -F 'ftp' '{{print $2}}' | sed '/^ *$/d'| uniq | awk '{{print $1}}'").read().rstrip()
                            # user_ftp = os.popen("cat /tmp/ont_sin_perfil.txt | awk -F 'ftp' '{{print $2}}' | sed '/^ *$/d'| uniq | awk '{{print $2}}'").read().rstrip()
                            # pass_ftp = os.popen("cat /tmp/ont_sin_perfil.txt | awk -F 'ftp' '{{print $2}}' | sed '/^ *$/d'| uniq | awk '{{print $3}}'").read().rstrip()
                            # telconn = pexpect.spawn('telnet {}'.format(ip_olt))
                            # telconn.timeout=30
                            # telconn.expect( ".* login:" )
                            # telconn.send("{}".format(user_olt) + "\r")
                            # telconn.expect("Password:")
                            # telconn.send( "{}".format(pass_olt) + "\r")
                            # telconn.expect(".*>")
                            # telconn.send("ena" + "\r")             
                            # telconn.expect("Password:")
                            # telconn.send( "{}".format(ena_olt) + "\r")
                            # telconn.expect(".*#")
                            # telconn.send("conf t" + "\r")
                            # telconn.expect(".*#")
                            # telconn.send("gp" + "\r")
                            # telconn.expect(".*#")
                            # telconn.send("gpon-olt {}".format(GPON) + "\r")
                            # telconn.expect(".*#")
                            # telconn.send("onu upgrade %s %s ftp %s %s %s " % (check_firmware_ont,firm_nuevo,ip_ftp,user_ftp,pass_ftp) + "\r")
                            # telconn.expect(".*#")
                            # telconn.send("end" + "\r")
                            # telconn.expect(".*#")
                            # telconn.send( "exit" + "\r")
                            # print("%s[+] Finalizamos la carga de firmware. Para ver el proceso puedes usar la opcion 9 del menu principal nuevamente" % verde)

                            #Tools().banner(hostname)
                            #main(hostname)
                elif confirmar == "n":
                    final = False
                    again = True
                    while again:
                        validar = input("%s[*] Quieres ver el firmware de otra ONT ? (s/n): %s" % (amarillo,reset_color))
                        if validar == "s":
                            again = False
                            Opciones().actualizar_firmware(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
                        elif validar == "n":
                            again = False
                            Tools().continuar(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
                            # print("%s[*] Muchas gracias por usar el script!" % magenta)
                            # Tools().clear_pantalla()
                            # Tools().banner(hostname)
                            # main(hostname)
                        else:
                            print("%s[-] La opcion disponible es 's' o 'n'%s" % (amarillo,reset_color))
                else:
                    print("%s[-] La opcion disponible es 's' o 'n'%s" % (amarillo,reset_color))

    #10
    def planes_olt(self,ip_olt,user_olt,pass_olt,ena_olt,name_olt):
        def buscar_plan_olt(ip_olt,user_olt,pass_olt,ena_olt,lista_planes,eleccion):
            if eleccion == "0":
                while True:
                    buscar_plan = input("%s[*] Escribe el plan que deseas buscar debe ser exacto (quit para ver planes): %s" % (cyan,reset_color))
                    if buscar_plan == "quit":
                        Opciones().planes_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
                        break
                    elif buscar_plan in lista_planes.values():
                        plan_encontrado =list(lista_planes.keys())[list(lista_planes.values()).index(buscar_plan)]        
                        lista_final = lista_planes[plan_encontrado]
                        break
                    elif buscar_plan == "":
                        print("%s[-] Debes ingresar un plan, intenta de nuevo" % amarillo)
                    else:
                        print("%s[-] Ese plan no esta en la lista, intenta de nuevo" % rojo)
            else:
                lista_final = lista_planes[eleccion]
            total_plan = os.popen("cat /tmp/expect-olt.txt | awk '{print $1,$3,$15}' | grep -v Profile | tr '\015' '\n' | sed '/^ *$/d' | grep -w 'o%s$' | wc -l " % (lista_final)).read()
            total_plan = total_plan.splitlines()
            total_plan = "".join(total_plan)
            print("%s[+] Encontramos un total de %s ont con el plan %s%s " % (verde,total_plan,blanco,lista_final))
            lista_mac = []
            lista_macs = []
            mac_ont = os.popen("cat /tmp/expect-olt.txt | awk '{print $1,$3,$7,$15}' | grep -v Profile | tr '\015' '\n' | sed '/^ *$/d' | grep -w 'o%s$' " %(lista_final)).read()
            mac_ont = mac_ont.splitlines()
            for mac in mac_ont:
                macs = mac.split(" ")
                lista_mac.append(macs)
            for indx in range(len(lista_mac)):
                lista_macs.append([lista_mac[indx][0],lista_mac[indx][1],lista_mac[indx][2],lista_mac[indx][3]])
            print("%s[+] Estas son las Mac :%s\n" % (verde,blanco))
            print("placa\tID\t    MAC\t\t  PLAN%s" % blanco)
            for i in lista_macs:
                mac = i[0] + '\t' + i[1] +'\t'+ i[2] + '\t' + i[3]
                print(mac)
            reiniciar = True
            while reiniciar:
                reiniciar_plan = input("%s\n[*] Quieres reiniciar todas las ONT ? (s/n) %s: " %(amarillo,reset_color))
                reiniciar_plan = reiniciar_plan.lower()
                if reiniciar_plan == "s":
                    while True:
                        reiniciar_plan = input("%s[*] Debes ingresar la palabra 'confirmar' para avanzar con el reinicio o 'quit' para salir: %s" % (cyan,reset_color))
                        reiniciar_plan = reiniciar_plan.lower()
                        if reiniciar_plan == "quit":
                            Opciones().planes_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
                        elif reiniciar_plan == "confirmar":
                            reiniciar = False
                            Tools().clear_pantalla()
                            Tools().banner(hostname)
                            list_placa_olt = []
                            placa_olt = os.popen("cat /tmp/expect-olt.txt | awk '{print $1,$3,$15}' | grep -v Profile | tr '\015' '\n' | sed '/^ *$/d' | grep -w o%s$ | awk '{print $1}' | uniq" %(lista_final) ).read()
                            placa_olt = placa_olt.splitlines()
                            for placa in placa_olt:
                                if placa not in list_placa_olt:
                                    list_placa_olt.append(placa)
                            for GPON in list_placa_olt:
                                id_reset_ont = os.popen("cat /tmp/expect-olt.txt | awk '{print $1,$3,$15}' | grep -v Profile | tr '\015' '\n' | sed '/^ *$/d' | grep -w o%s$ | grep %s | awk '{print $2}'" % (lista_final,GPON)).read()
                                id_reset_ont = id_reset_ont.splitlines()
                                lista_reset = []
                                for ont_reset in id_reset_ont:
                                    if ont_reset not in lista_reset:
                                        lista_reset.append(ont_reset)
                                reset_ont = ",".join(lista_reset)
                                limpiar_ont = ""
                                firmware_ont = ""
                                descripcion_ont = ""
                                ont_sin_profile = ""
                                print("%s[+] Reiniciando ONT/s en %s con perfil %s" % (verde,GPON,lista_final))
                                telconn = pexpect.spawn('telnet {}'.format(ip_olt))
                                telconn.logfile = open ('/tmp/sacamos_inactivas.txt' , 'ab' )
                                telconn.expect( ".* login:" )
                                telconn.send("{}".format(user_olt) + "\r")
                                telconn.expect("Password:")
                                telconn.send( "{}".format(pass_olt) + "\r")
                                telconn.expect(".*>")
                                telconn.send("ena" + "\r")             
                                telconn.expect("Password:")
                                telconn.send( "{}".format(ena_olt) + "\r")
                                telconn.expect(".*#")
                                telconn.send("terminal length 0" + "\r")
                                telconn.expect(".*#")
                                telconn.send("conf t" + "\r")
                                telconn.expect(".*#")
                                telconn.send("gp" + "\r")
                                telconn.expect(".*#")
                                telconn.send("gpon-olt {}".format(GPON) + "\r")
                                telconn.expect(".*#")        
                                telconn.send("onu reset {}".format(reset_ont) + "\r")
                                telconn.expect(".*#")                            
                                #recorrer_placas(ip_olt,user_olt,pass_olt,ena_olt,GPON,limpiar_ont,reset_ont,firmware_ont,descripcion_ont,ont_sin_profile)
                            break
                        elif reiniciar_plan == "":
                            print("%s[-] No ingresaste nada " % amarillo)
                            reiniciar = False
                        else:
                            print("%s[-] Ingresar confirmar para continuar o quit para ver planes de nuevo: " % amarillo)
                            reiniciar = False
                elif reiniciar_plan == "n":
                    reiniciar = False
                    print("%s[+] Mostrando nuevamente los planes..." % cyan)
                    time.sleep(1.5)
                    Opciones().planes_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
                else:
                    print("%s[-] La opcion disponible es 's' o 'n'" % amarillo)

        Tools().clear_pantalla()
        Tools().banner(hostname)
        print("%s[+] Estos son los planes encontrados:\n%s" % (cyan,blanco))
        planes = os.popen("cat /tmp/expect-olt.txt | awk '{print $15}' | grep -v Profile | tr '\015' '\n' | sed '/^ *$/d' | uniq| sed 's/^o//g'").read()
        planes = planes.splitlines()
        opcion = 1
        lista_planes = {}
        lista_planes2 = []
        total_ont = 0
        for plan in planes:
            plan = plan.rstrip()
            if plan not in lista_planes.values():
                plan_total = os.popen("cat /tmp/expect-olt.txt | awk '{print $1,$3,$15}' | grep -v Profile | tr '\015' '\n' | sed '/^ *$/d' | grep -w 'o%s$' | wc -l " %(plan) ).read()
                plan_total = plan_total.splitlines()
                plan_total = "".join(plan_total)
                lista_planes2.append([plan,plan_total])
                total_ont+=int(plan_total)
                #print("%s)%s\t%s %s %s" %(opcion,plan.ljust(30),verde,plan_total,blanco))
                lista_planes["%s"%opcion] = "%s" % plan
                opcion += 1
        opcion = 1
        lista_planes = {}
        lista_ordenada_planes = sorted(lista_planes2, key=lambda x: int(x[-1]))
        print("OPCIONES\t\t\t  ONT POR PLAN\n")
        for elemento in lista_ordenada_planes:
            print("%s)%s\t%s%s%s" % (opcion,elemento[0].ljust(30),verde,elemento[1],blanco))
            lista_planes["%s"%opcion] = elemento[0]
            opcion += 1
        print("\nTotal de ont activas: %s" % total_ont)
        validar = True
        while validar:
                eleccion = input("\n%s[*] Ingresa una opcion del menu o 0 para introducir manualmente el plan (quit para salir): %s" % (cyan,reset_color))
                eleccion = eleccion.lower()
                if eleccion == "quit":
                    validar = False
                    Tools().clear_pantalla()
                    Tools().banner(hostname)
                    Main().menu(hostname,ip_olt,user_olt,pass_olt,ena_olt,name_olt)
                    #main(hostname)
                elif eleccion == "0":
                    validar = False
                    buscar_plan_olt(ip_olt,user_olt,pass_olt,ena_olt,lista_planes,eleccion)
                elif eleccion in lista_planes.keys():
                    #validar = False
                    validar = False
                    buscar_plan_olt(ip_olt,user_olt,pass_olt,ena_olt,lista_planes,eleccion)
                elif eleccion in lista_planes.values():
                    validar = False
                    eleccion =list(lista_planes.keys())[list(lista_planes.values()).index(eleccion)]
                    buscar_plan_olt(ip_olt,user_olt,pass_olt,ena_olt,lista_planes,eleccion)
                else:
                    print("%s[-] Debes ingresar una opcion o 0 para ingresar manualmente el plan" % amarillo)

    #11
    def ont_noip(self,ip_olt,user_olt,pass_olt,ena_olt,name_olt):
            list_placa_olt,modelo_olt,list_modelos_ont = Olt().pre_placas_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
            os.system(">/tmp/ont_sin_ip.txt")
            ont_elegida = ""
            parametro = "factory_reset"
            check_ont = os.popen("cat /tmp/ont_models.txt | grep \"LD420-10R\|LD421-21W\" ").read()
            if check_ont:
                Tools().clear_pantalla()
                Tools().banner(hostname)
                print("%s[+] Comenzando chequeo de IP gestion para ont LD420-10R ... " % verde)
                Olt().recorrer_placas(ip_olt,user_olt,pass_olt,ena_olt,list_placa_olt,ont_elegida,parametro,modelo_olt)
                Tools().clear_pantalla()
                if not os.stat("/tmp/ont_sin_ip.txt").st_size == 0:
                    print("%s"%blanco)
                    os.system("cat /tmp/ont_sin_ip.txt")
                    print("%s"%reset_color)
                    os.remove("/tmp/ont_sin_ip.txt")
                else:
                    Tools().clear_pantalla()
                    Tools().banner(hostname)
                    print("%s\n[+] No hay ONT sin IP. Felicidades!!" % verde)
            else:
                Tools().clear_pantalla()
                Tools().banner(hostname)
                print("\n%s[*] No hay ONT LD420-10R en esta OLT" % amarillo)

    #12
    def check_firmware_ont(self,ip_olt,user_olt,pass_olt,ena_olt,name_olt):
            list_placa_olt,modelo_olt,list_modelos_ont = Olt().pre_placas_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
            dicc_modelos,list_modelos3 = Olt().modelos_ont(ip_olt,list_modelos_ont)
            try:
                    validar_opcion = True
                    while validar_opcion:
                            eleccion = input("\n%s[*] Por favor seleccione una opcion (quit para salir): %s" % (cyan,reset_color))
                            if eleccion == "quit":
                                    validar_opcion = False
                                    Tools().clear_pantalla()
                                    Tools().banner(hostname)
                                    print("%s[-] Abortando por usuario...%s" % (magenta,reset_color))
                                    Tools().borrar_archivos()
                                    Main().menu(hostname,ip_olt,user_olt,pass_olt,ena_olt,name_olt)                                      
                            elif eleccion in list_modelos3:
                                    validar_opcion = False
                                    ont_elegida = dicc_modelos["{}".format(eleccion)]
                                    os.system(">/tmp/sacamos_inactivas.txt")
                            else:
                                    print("%s[-] Esa no es una opcion del menu, intente de nuevo%s" % (amarillo,reset_color))
                    while True:
                        version_firmware = input("%s[*] Cual es la version de firmware que quieres comprobar de la ont %s :%s " % (cyan,ont_elegida,reset_color))
                        if version_firmware == "":
                            print("%s[-] Debes ingresar algo (-.-) " % amarillo)        
                        else:
                            break
            
                    while True:
                        inc_or_exc = input("%s[*] Ingresa '%si%s' para incluir o '%se%s' para excluir la version de firmware:%s " % (cyan,verde,cyan,rojo,cyan,reset_color))
                        inc_or_exc = inc_or_exc.lower()            
                        if inc_or_exc == "i" or inc_or_exc == "e":
                            break
                        else:
                            print("%s[-] Debes ingresar 'i' para incluir o 'e' para excluir" % amarillo)        
                    print("%s[+] Comenzando comprobacion, aguarde ..." % verde)
                    print("%s[+] Chequeando firmware de ONT %s %s" % (verde,ont_elegida,reset_color))
                    parametro = "firmware_ont"
                    Olt().recorrer_placas(ip_olt,user_olt,pass_olt,ena_olt,list_placa_olt,ont_elegida,parametro,modelo_olt)
                    Tools().clear_pantalla()
                    Tools().banner(hostname)
                    if inc_or_exc == "i":
                        check_version_firmware = os.popen("cat /tmp/sacamos_inactivas.txt | grep '|' | grep %s > /tmp/sacamos_inactivas2.txt" % version_firmware).read()
                        if os.stat("/tmp/sacamos_inactivas2.txt").st_size != 0:
                            print("%s[+] Estos son los firmware de ONT {} con version %s :\n%s".format(ont_elegida) % (cyan,version_firmware,blanco))
                            os.system("cat /tmp/sacamos_inactivas.txt | grep '|' | egrep '(OLT|%s)' > /tmp/sacamos_inactivas2.txt" % version_firmware)
                            total_ont = os.popen("cat /tmp/sacamos_inactivas2.txt  | grep '|' | grep -v OLT | wc -l").read().strip()
                            os.system("sed '/OLT/i------------------------------------------------------------------------------------------------------' /tmp/sacamos_inactivas2.txt")  
                            otra_version_firm = os.popen("cat /tmp/sacamos_inactivas.txt | grep '|' | egrep -v '(OLT|%s)' | wc -l" % version_firmware).read().strip()
                            if otra_version_firm == "0":
                                print("\n%s[+] Todas las ONT %s tienen ese firmware. Total de ONT %s " % (verde,ont_elegida,total_ont))
                            else:
                                print("\n%s[*] Hay %s%s%s con ese firmware, pero hay %s ONT %s%s%s que no tienen esa version." % (amarillo,verde,total_ont,amarillo,otra_version_firm,verde,ont_elegida,amarillo))
                        else:
                            print("%s[-] No hay versiones %s para las ont %s " % (rojo,version_firmware,ont_elegida))
            
                    elif inc_or_exc == "e":
                        check_version_firmware = os.popen("cat /tmp/sacamos_inactivas.txt | grep '|' | grep  %s > /tmp/sacamos_inactivas2.txt" % version_firmware).read()
                        if os.stat("/tmp/sacamos_inactivas2.txt").st_size != 0:
                            print("%s[+] Estos son los firmware de ONT {} con version excluida %s :\n%s".format(ont_elegida) % (cyan,version_firmware,blanco))
                            os.system("cat /tmp/sacamos_inactivas.txt | grep '|' | egrep -v '(OLT|%s)' > /tmp/sacamos_inactivas2.txt" % version_firmware)
                            total_ont = os.popen("cat /tmp/sacamos_inactivas2.txt  | grep '|' | grep -v OLT | wc -l").read().strip()
                            os.system("sed '/OLT/i------------------------------------------------------------------------------------------------------' /tmp/sacamos_inactivas2.txt")  
                            otra_version_firm = os.popen("cat /tmp/sacamos_inactivas.txt | grep '|' | grep %s | wc -l" % version_firmware).read().strip()
                            if otra_version_firm == "0":
                                print("\n%s[-] No hay versiones %s para las ont %s . Se revisaron %s ONT de ese modelo" % (rojo,version_firmware,ont_elegida,total_ont))
                                #print("\n%s[+] Todas las ONT %s tienen ese firmware. Total de ONT %s " % (verde,ont_elegida,total_ont))
                            else:
                                print("\n%s[*] Hay %s%s%s que no tienen el firmware %s , pero hay %s ONT %s%s%s que si tienen esa version." % (amarillo,verde,total_ont,amarillo,version_firmware,otra_version_firm,verde,ont_elegida,amarillo))       
                        else:
                            print("%s[-] No hay versiones a excluir %s para las ont %s " % (rojo,version_firmware,ont_elegida))

            except KeyboardInterrupt:
                    Tools().clear_pantalla()
                    print("%s\n[-]Detectamos interrupcion por teclado (ctrl+c).Empezando nuevamente..." % rojo)
                    Tools().menu_again(hostname)    

    #13
    def mac_list(self,ip_olt,user_olt,pass_olt,ena_olt,name_olt):
            list_placa_olt,modelo_olt,list_modelos_ont = Olt().pre_placas_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
            dicc_modelos,list_modelos3 = Olt().modelos_ont(ip_olt,list_modelos_ont)
            try:
                    validar_opcion = True
                    while validar_opcion:
                            eleccion = input("\n%s[*] Por favor seleccione una opcion (quit para salir): %s" % (cyan,reset_color))
                            if eleccion == "quit":
                                    validar_opcion = False
                                    Tools().clear_pantalla()
                                    Tools().banner(hostname)
                                    print("%s[-] Abortando por usuario...%s" % (magenta,reset_color))
                                    Tools().borrar_archivos()
                                    Main().menu(hostname,ip_olt,user_olt,pass_olt,ena_olt,name_olt)                                      
                            elif eleccion in list_modelos3:
                                    validar_opcion = False
                                    ont_elegida = dicc_modelos["{}".format(eleccion)]
                                    os.system(">/tmp/sacamos_inactivas.txt")
                            else:
                                    print("%s[-] Esa no es una opcion del menu, intente de nuevo%s" % (amarillo,reset_color))
            except KeyboardInterrupt:
                    Tools().clear_pantalla()
                    print("%s\n[-]Detectamos interrupcion por teclado (ctrl+c).Empezando nuevamente..." % rojo)
                    Tools().menu_again(hostname)   
            print("%s[+] Comenzando comprobacion, aguarde ..." % verde)
            print("%s[+] Extrayendo Info de ONTS %s" % (verde,reset_color))
            parametro = "info"
            Olt().recorrer_placas(ip_olt,user_olt,pass_olt,ena_olt,list_placa_olt,ont_elegida,parametro,modelo_olt)
            Tools().clear_pantalla()
            Tools().banner(hostname)
            print("%s[+] Esta son las MAC de ONT {}:\n%s".format(ont_elegida) % (cyan,blanco))
            #os.system('cat /tmp/sacamos_inactivas.txt | grep "|" | grep Active | awk \'{{ printf "\\t%s %s \\t%s \\t%s %s \\n", $1,$2,$3,$4,$7 }}\' > /tmp/sacamos_inactivas2.txt ; cat /tmp/sacamos_inactivas2.txt | sed \'1G\' |awk \'!x[$0]++\'')
            os.system('cat /tmp/sacamos_inactivas.txt | grep "|" | grep Active | awk \'{{ printf "\\t%s\\t%s \\t%s \\t%s %s \\n", $1,$2,$3,$4,$7 }}\' > /tmp/sacamos_inactivas2.txt ; cat /tmp/sacamos_inactivas2.txt')
            #os.system("sed '/OLT/i------------------------------------------------------------------------------------------------------' /tmp/sacamos_inactivas2.txt")
            total_ont = os.popen("cat /tmp/sacamos_inactivas2.txt |awk '!x[$0]++'| grep -v OLT | wc -l").read()
            print("\n%s[*] Cantidad TOTAL: %s" % (verde,total_ont))

    #14
    def download_fail(self,ip_olt,user_olt,pass_olt,ena_olt,name_olt):
            list_placa_olt,modelo_olt,list_modelos_ont = Olt().pre_placas_olt(ip_olt,user_olt,pass_olt,ena_olt,name_olt)
            total_ont = os.popen("cat /tmp/ont_models.txt | grep -i fail | awk '{{print $1,$3}}' | wc -l").read().strip()
            if total_ont != "0":
                parametro = "reset_ont"
                ont_elegida = "ont_donwload_fail"       
                Tools().clear_pantalla()
                Tools().banner(hostname)
                print ("%s[+] Comenzando reinicio, aguarde..." % (verde) ) 
                Olt().recorrer_placas(ip_olt,user_olt,pass_olt,ena_olt,list_placa_olt,ont_elegida,parametro,modelo_olt)
                Tools().clear_pantalla()
                Tools().banner(hostname)
                total_ont = os.popen("cat /tmp/ont_models.txt | grep -i fail | awk '{{print $1,$3}}' | wc -l").read().strip()
                print("%s[+] Se reiniciaron %s ont en estado DOWNLOAD FAIL \n%s" % (cyan,total_ont,blanco))
            else:
                Tools().clear_pantalla()
                Tools().banner(hostname)
                print("%s[+] No existen ont en estado DONWLOAD FAIL \n%s" % (cyan,blanco))

    #15
    def bkp_olt(self,ip_olt,user_olt,pass_olt,ena_olt,name_olt):
        dateF = os.popen("date +%F").read().strip()
        mysql_pass = "From\$hell#2003"
        ip_apros = os.popen("mysql -uroot -h 127.0.0.1 -p%s -e 'select value_parameter from aprosftthdata.parameter where name_parameter=\"ftp_ip\"' 2>&1 | egrep -v '(Warning|value_parameter)'" % mysql_pass).read().strip()
        modelo_olt = Olt().modelo_olt()
        name_olt = name_olt.strip()
        nombreBkpOlt = "running_%s_%s_%s" %(ip_olt,modelo_olt,dateF)
        bkpOlt= pexpect.spawn("telnet %s" % ip_olt)
        bkpOlt.expect( ".* login:" )
        bkpOlt.send( "%s" % user_olt + "\r" )
        bkpOlt.expect("Password:")
        bkpOlt.send( "{}".format(pass_olt) + "\r")
        bkpOlt.expect(".*>")
        bkpOlt.send("ena" + "\r")
        bkpOlt.expect("Password:")
        bkpOlt.send( "%s" % ena_olt + "\r" )
        bkpOlt.expect(".#")
        bkpOlt.send("write memory" + "\r")
        bkpOlt.expect(".#")
        bkpOlt.send("copy ftp config upload startup-config" + "\r")
        bkpOlt.expect(".*: ")
        bkpOlt.send("%s" % (ip_apros) + "\r")
        bkpOlt.expect(".*[running]")
        bkpOlt.send("%s" %(nombreBkpOlt) + "\r")
        bkpOlt.expect(".*Name :")
        bkpOlt.send("furukawa" + "\r")
        bkpOlt.expect(".*: ")
        bkpOlt.send("E8f48542" + "\r")
        bkpOlt.expect(".#")
        bkpOlt.send("exit" +"\r")

        if os.path.exists("/home/furukawa/%s"% (nombreBkpOlt)):
            print("%s[+] Backup finalizado OK!" % (verde))
            print("%s[+] Se encuentra en : /home/furukawa/%s%s\n" %(blanco,nombreBkpOlt,reset_color))
        else:
            print("%s[-] >>>> ERROR DE BACKUP <<<<<  VERIFIQUE LO SUCEDIDO O REPORTELO.%s"%(rojo,reset_color))

#################################### FIN ######################################



######################### CHANGE LOG ##########################################

#version 1.10
#Se integra script opciones_huawei para OLT_HUAWEI
#Se agrega opcion 15 Backup OLT 


# version 1.09
# Se agrega la opcion 14 Reset de ONT DOWNLOAD FAIL
# Se agrega funcion pre-placas muy importante 
# Se agrega funcion modelos_ont()
# Se agrega validacion de familias OLT para ejecucion de comandos


# version 1.08
# Se agrega la comprobacion de modelos de OLT por modelo
# Se mejora la opcion 12 con inclusion y exclusion de firmware
# Se usa tipo sw case para el menu de opciones general
# Se agrega la opcion 13 Ver Mac por modelo de ONT


# version 1.07
# Se mueven los script en el dir /opciones_ont
# Se agrega alias en los servers para ejecutar solamente con opciones_ont
# Se agrega validacion para que solo corra 2 script de opciones_ont


# version  1.06
# Se agrega opcion 12)Comprobar firmware de ONT por modelo
# Correccion extraccion de datos OLT
# Cambio de files a /tmp

# version 1.05
# Modificacion del resumen opcion 3)Ver cantidad total de ONT ACTIVAS
# Se corrige opcion Ver/Actualizar Firware de ONT por MAC 


# version 1.04
# Se agrega opcion 11)Factory Reset ONT sin ip de gestion
# Se fixea y optimiza funcion placas y funcion recorrer placas lo cual lo hace mucho mas rapido ahora.
# Se confecciona banner para mostrar la ubicacion en la que esta trabajando
# Se mejora opcion 3)Ver cantidad total de ONT ACTIVAS


# version 1.03
# Cambio de banner
# Chequeo si el server tiene salida a internet
# Chequeo de la version de Python y modulos necesarios
# Se agrega "tecla para continuar"

# version 1.02
# Se grega opcion 9 Ver/Actualizar Firmware poe modelo 
# Se agrega opcion 10 Ver/Reiniciar ONT por plan
# Se corrige la opcion 6 Fuera de rango 

###################################################################################################################

if __name__ == '__main__':
    import os,sys
    # usuarios_cantidad = 2
    # if not os.path.exists("/opciones_ont/opciones_ont.py"):
    #     print("%s\n[*] No existe el script principal en este server. Pongase en contacto con su autor Emilio Airut\n%s" % (amarillo,reset_color) )
    #     sys.exit(0)
    # pid = os.popen("ps ax | grep /opciones_ont/opciones_ont.py | grep -v grep | awk '{print $1}' | wc -l ").read().strip()
    # pid_kill = os.popen("ps ax | grep -m1 /opciones_ont/opciones_ont.py | grep -v grep | awk '{print $1}'").read().strip()
    # if int(pid) > usuarios_cantidad :
    #     print("%s\n[-] Existe un OPCIONES_ONT corriendo actualmente. Si quiere forzar el cierre del mismo deberá realizar %skill -9 %s\n" % (rojo,amarillo,pid_kill))
    #     sys.exit(0)
    hostname = os.popen("hostname").read().rstrip().upper()
    Tools().clear_pantalla()
    if sys.version_info < (3,4,0):
      sys.stderr.write("\n%s[*] Debes tener python3.4, sino lo tienes debes instalarlo por tu cuenta\n%s\n" % (amarillo,reset_color))
      sys.exit(0)
    else:
        Tools().banner(hostname)
        if Tools().install() == "Sin salida":
            import sys
            print("\n%s[-] Debes tener internet para instalar los modulos requeridos\n%s" % (amarillo,reset_color))
            sys.exit(0)
        else:
            import sys,pexpect,time,subprocess,ipaddress

            print("\n%s[+] Cargando los modulos necesarios\n" % verde)
            time.sleep(0.5)
            Tools().clear_pantalla()
            Tools().banner(hostname)
            Main().main(hostname)  
